local Library = loadstring(game:HttpGet("https://gitlab.com/blinxduh/cerberus/-/raw/main/Library.lua", true))()

local Window = Library:BeginWindow({
    ToggleKey = "RightShift",
    DestroyKey = "RightBracket"
})

local Page1 = Window:BeginPage("Page 1")
local Page2 = Window:BeginPage("Page 2")

local page1_section1 = Page1:CreateSection("Section 1")

local action = page1_section1:CreateAction({
    Name = "Action 1",
    OnClick = function()
        print("click on action")
    end
})

local check = page1_section1:CreateCheckbox({
    Name = "Checkbox 1",
    Default = false,
    OnChanged = function(value) 
        print(value)
    end
})

local switch = page1_section1:CreateSwitch({
    Name = "Switch 1",
    Default = true,
    OnChanged = function(value) 
        print(value)
    end
})

local slider_wk = page1_section1:CreateSlider({
    Name = "WalkSpeed",
    Range = { 16, 500 },
    Default = { 16 },
    OnChanged = function(value) 
        game:GetService("Players").LocalPlayer.Character.Humanoid.WalkSpeed = value
    end
})

local reset_wk = page1_section1:CreateAction({
    Name = "Reset WalkSpeed",
    OnClick = function() 
        slider_wk:Set(16)
    end
})

local slider_jp = page1_section1:CreateSlider({
    Name = "JumpPower",
    Range = { 50, 500 },
    Default = { 50 },
    OnChanged = function(value) 
        game:GetService("Players").LocalPlayer.Character.Humanoid.JumpPower = value
    end
})

local dropdown_1 = page1_section1:CreateDropdown({
    Name = "Event Type",
    Items = { "PRE", "POST", "DEFER" },
    OnSelected = function(item) 
        print(item, "Selected")
    end
})
local remove = page1_section1:CreateAction({
    Name = "Remove Item (DEFER)",
    OnClick = function()
        dropdown_1:RemoveItem("DEFER") 
    end
})

local multi_dropdown1 = page1_section1:CreateMultiDropdown({
    Name = "Flags",
    Items = {
        ["Flag 1"] = true,
        ["Flag 2"] = false,
        ["Flag 3"] = true,
        ["Flag 4"] = false,
    },
    OnChanged = function(flag, value) 
        print(flag, ":", value)
    end
})

