local Library = {Connections={}}
local Support = {}

local CoreGui = cloneref(game:GetService("CoreGui"))
local Client = cloneref(game:GetService("Players")).LocalPlayer
local Tween = cloneref(game:GetService("TweenService"))
local UIS = cloneref(game:GetService("UserInputService"))
local RS = cloneref(game:GetService("RunService"))

local task = task
local table = table
local math = math
local huge = math.huge
local cos = math.cos

function Support:Base64(Input_String) 
    local base64chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
    local result = {}
    local len = #Input_String
    local i = 1
    while i <= len do
        local char1 = string.byte(Input_String, i)
        local char2, char3, char4 = 0, 0, 0
        local index1, index2, index3, index4 = 0, 0, 0, 0

        index1 = bit32.band(char1, 0x3f)
        result[#result+1] = string.sub(base64chars, index1+1, index1+1)

        if i+1 <= len then
            char2 = string.byte(Input_String, i+1)
            index2 = bit32.rshift(char1, 6)
            index3 = bit32.band(bit32.bor(bit32.lshift(bit32.band(char1, 0x03), 4), bit32.rshift(char2, 4)), 0x3f)
            result[#result+1] = string.sub(base64chars, index2+1, index2+1)
            result[#result+1] = string.sub(base64chars, index3+1, index3+1)
        else
            index2 = bit32.rshift(char1, 6)
            result[#result+1] = string.sub(base64chars, index2+1, index2+1)
            result[#result+1] = '='
        end

        if i+2 <= len then
            char3 = string.byte(Input_String, i+2)
            index3 = bit32.band(bit32.bor(bit32.lshift(bit32.band(char2, 0x0f), 2), bit32.rshift(char3, 6)), 0x3f)
            result[#result+1] = string.sub(base64chars, index3+1, index3+1)
        else
            index3 = bit32.lshift(bit32.band(char2, 0x0f), 2)
            result[#result+1] = string.sub(base64chars, index3+1, index3+1)
            result[#result+1] = '='
        end

        if i+3 <= len then
            char4 = string.byte(Input_String, i+3)
            index4 = bit32.band(char4, 0x3f)
            result[#result+1] = string.sub(base64chars, index4+1, index4+1)
        else
            result[#result+1] = '='
        end

        i = i + 3
    end

    return table.concat(result)
end

function Support:ProtectGui(object)
    if get_hidden_gui or gethui then
        local hiddenUI = get_hidden_gui or gethui
        object.Parent = hiddenUI()
    elseif (not is_sirhurt_closure) and (syn and syn.protect_gui) then
        syn.protect_gui(object)
    elseif CoreGui:FindFirstChild('RobloxGui') then
        object.Parent = CoreGui:FindFirstChild('RobloxGui')
    else
        object.Parent = CoreGui
        messagebox("Cannot protect gui!", "Security Flaw Detected", 0)
        --object.Parent = Client:WaitForChild("PlayerGui")
    end
end

function Support:CreateStroke(Object, Properties) 
    local Stroke = Instance.new("UIStroke", Object);
    Stroke.ApplyStrokeMode = Enum.ApplyStrokeMode.Contextual
    Stroke.Color = Properties.Color
    Stroke.Thickness = Properties.Thickness
    Stroke.Transparency = Properties.Transparency
    Stroke.Enabled = Properties.Enabled
    Stroke.Name = ("%s_Stroke"):format(Object.Name)

    return Stroke
end

function Support:CreateButton(Object)
    local Button = Instance.new("TextButton", Object)
    
    Button.Name = "Button"
    Button.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    Button.BackgroundTransparency = 1.000
    Button.Size = UDim2.new(1, 0, 1, 0)
    Button.Font = Enum.Font.SourceSans
    Button.Text = ""
    Button.TextColor3 = Color3.fromRGB(0, 0, 0)
    Button.TextSize = 14.000
    return Button
end

function Library:BeginWindow(WindowOptions) 
    local WindowOptions = WindowOptions or {
        Title = WindowOptions.Title or "Title",
        ToggleKey = WindowOptions.ToggleKey or "RightShift",
        DestroyKey = WindowOptions.DestroyKey or "RightBracket"
    }

    Library.Name = Support:Base64(tostring(Client.UserId))
    Library.Font = Font.fromId(12187365364)

    for i, obj in pairs(CoreGui:GetChildren()) do 
        if (obj:IsA("ScreenGui") and obj.Name == Library.Name) then
            obj:Destroy()
        end
    end

    local Interface = Instance.new("ScreenGui")
    Interface.Name = Library.Name
    Support:ProtectGui(Interface)

    local Window                = Instance.new("Frame")
    local WindowCorner          = Instance.new("UICorner")
    local WindowTabs            = Instance.new("Frame")
    local WindowTabsLayout      = Instance.new("UIListLayout")
    local WindowTabsPadding     = Instance.new("UIPadding")
    local WindowIcon            = Instance.new("ImageLabel")
    local WindowTabsText        = Instance.new("TextLabel")
    local WindowTabsTextPadding = Instance.new("UIPadding")
    local WindowSeperator       = Instance.new("Frame")
    local WindowTabDisplay      = Instance.new("Frame")
    local TabSeperator          = Instance.new("Frame")
    local WindowPanel           = Instance.new("Frame")
    local WindowPattern = Instance.new("ImageLabel")

    WindowPattern.Name = "WindowPattern"
    WindowPattern.Parent = Window
    WindowPattern.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    WindowPattern.BackgroundTransparency = 1.000
    WindowPattern.Position = UDim2.new(0.00285722548, 0, 0, 0)
    WindowPattern.Size = UDim2.new(0, 662, 0, 529)
    WindowPattern.Image = "rbxassetid://300134974"
    WindowPattern.ImageColor3 = Color3.fromRGB(18, 18, 18)
    WindowPattern.ScaleType = Enum.ScaleType.Tile
    WindowPattern.SliceCenter = Rect.new(0, 256, 0, 256)
    WindowPattern.TileSize = UDim2.new(0, 30, 0, 30)

    WindowPanel.Name = "WindowPanel"
    WindowPanel.Parent = WindowTabDisplay
    WindowPanel.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    WindowPanel.BackgroundTransparency = 1.000
    WindowPanel.Position = UDim2.new(-0.224952742, 0, 0.00188679248, 0)
    WindowPanel.Size = UDim2.new(0, 647, 0, 30)

    Window.Name                 = "Window"
    Window.Parent               = Interface
    Window.BackgroundColor3     = Color3.fromRGB(16, 16, 16)
    Window.Position             = UDim2.new(0.0769998655, 0, 0.125, 0)
    Window.Size                 = UDim2.new(0, 665, 0, 530)

    WindowCorner.CornerRadius   = UDim.new(0, 4)
    WindowCorner.Name           = "WindowCorner"
    WindowCorner.Parent         = Window

    WindowTabs.Name             = "WindowTabs"
    WindowTabs.Parent           = Window
    WindowTabs.BackgroundColor3 = Color3.fromRGB(76, 76, 76)
    WindowTabs.BackgroundTransparency = 1.000
    WindowTabs.Size             = UDim2.new(0, 130, 1, 0)
    WindowTabs.Position         = UDim2.new(0, 0, 0, 0)

    WindowTabsLayout.Name       = "WindowTabsLayout"
    WindowTabsLayout.Parent     = WindowTabs
    WindowTabsLayout.HorizontalAlignment = Enum.HorizontalAlignment.Center
    WindowTabsLayout.SortOrder  = Enum.SortOrder.LayoutOrder
    WindowTabsLayout.Padding    = UDim.new(0, 6)

    WindowTabsPadding.Name      = "WindowTabsPadding"
    WindowTabsPadding.Parent    = WindowTabs
    WindowTabsPadding.PaddingTop= UDim.new(0, 10)

    WindowIcon.Name             = "WindowIcon"
    WindowIcon.Parent           = WindowTabs
    WindowIcon.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    WindowIcon.BackgroundTransparency = 1.000
    WindowIcon.Size             = UDim2.new(0, 80, 0, 80)
    WindowIcon.Image            = "rbxassetid://13293530722"

    WindowTabsText.Name         = "WindowTabsText"
    WindowTabsText.Parent       = WindowTabs
    WindowTabsText.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    WindowTabsText.BackgroundTransparency = 1.000
    WindowTabsText.Size         = UDim2.new(0, 125, 0, 20)
    WindowTabsText.FontFace = Library.Font
    WindowTabsText.Text         = WindowOptions.Title
    WindowTabsText.TextColor3   = Color3.fromRGB(181, 181, 181)
    WindowTabsText.TextSize     = 17.000
    WindowTabsText.TextXAlignment = Enum.TextXAlignment.Left

    WindowTabsTextPadding.Name      = "WindowTabsTextPadding"
    WindowTabsTextPadding.Parent    = WindowTabsText
    WindowTabsTextPadding.PaddingLeft = UDim.new(0, 10)
    WindowTabsTextPadding.PaddingTop = UDim.new(0, -5)
    WindowTabsTextPadding.PaddingRight = UDim.new(0, 10)

    WindowSeperator.Name            = "WindowSeperator"
    WindowSeperator.Parent          = WindowPattern
    WindowSeperator.BackgroundColor3= Color3.fromRGB(44, 44, 44)
    WindowSeperator.BorderSizePixel = 0
    WindowSeperator.Size            = UDim2.new(0, 1, 1, 0)
    WindowSeperator.Position        = UDim2.new(0.189, 3,0, 0)
    
    WindowTabDisplay.Name           = "WindowTabDisplay"
    WindowTabDisplay.Parent         = WindowPattern
    WindowTabDisplay.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    WindowTabDisplay.BackgroundTransparency = 1.000
    WindowTabDisplay.Position       = UDim2.new(0.192, 0,-0.002, 0)
    WindowTabDisplay.Size           = UDim2.new(0.66, 100,1, 0)

    TabSeperator.Name = "TabSeperator"
    TabSeperator.Parent = WindowTabDisplay
    TabSeperator.AnchorPoint = Vector2.new(0.5, 0.5)
    TabSeperator.BackgroundColor3 = Color3.fromRGB(44, 44, 44)
    TabSeperator.BorderSizePixel = 0
    TabSeperator.Position = UDim2.new(0.5, 0, 0, 20)
    TabSeperator.Rotation = 90.000
    TabSeperator.Size = UDim2.new(0, 1, 0, 530)
    TabSeperator.BackgroundTransparency = 1
    
    local TabButtonContainer        = Instance.new("Frame")
    local TabButtonContainerLayout  = Instance.new("UIListLayout")

    TabButtonContainer.Name         = "TabButtonContainer"
    TabButtonContainer.Parent       = WindowTabs
    TabButtonContainer.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    TabButtonContainer.BackgroundTransparency = 1.000
    TabButtonContainer.Size         = UDim2.new(1, 0, 0, 410)

    TabButtonContainerLayout.Name   = "TabButtonContainerLayout"
    TabButtonContainerLayout.Parent = TabButtonContainer
    TabButtonContainerLayout.HorizontalAlignment = Enum.HorizontalAlignment.Center
    TabButtonContainerLayout.SortOrder = Enum.SortOrder.LayoutOrder

    local Pages = {}
    local PageHandler = {}

    function PageHandler:BeginPage(PageName) 
        local PageName = PageName or "Page"
        local PageClass = {
            Name = PageName,
            Showing = false,
            Page = nil,
            Update = nil
        }

        local DROPDOWN_ACTIVE = false

        local TabButton             = Instance.new("Frame")
        local TabButtonText         = Instance.new("TextLabel")
        local TabButtonTextPadding  = Instance.new("UIPadding")
        local TabButtonIndicator    = Instance.new("Frame")

        TabButton.Name              = "TabButton"
        TabButton.Parent            = TabButtonContainer
        TabButton.BackgroundColor3  = Color3.fromRGB(24, 24, 24)
        TabButton.BackgroundTransparency = 0.250
        TabButton.BorderSizePixel   = 0
        TabButton.Size              = UDim2.new(1, 0, 0, 35)

        TabButtonText.Name          = "TabButtonText"
        TabButtonText.Parent        = TabButton
        TabButtonText.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
        TabButtonText.BackgroundTransparency = 1.000
        TabButtonText.Size          = UDim2.new(1, 0, 1, 0)
        TabButtonText.FontFace      = Library.Font
        TabButtonText.Text          = (PageName)
        TabButtonText.TextColor3    = Color3.fromRGB(216, 216, 216)
        TabButtonText.TextSize      = 17.000    
        TabButtonText.TextXAlignment = Enum.TextXAlignment.Left

        TabButtonTextPadding.Name = "TabButtonTextPadding"
        TabButtonTextPadding.Parent = TabButtonText
        TabButtonTextPadding.PaddingLeft = UDim.new(0, 20)

        TabButtonIndicator.Name = "TabButtonIndicator"
        TabButtonIndicator.Parent = TabButton
        TabButtonIndicator.BackgroundColor3 = Color3.fromRGB(248, 72, 4)
        TabButtonIndicator.BorderSizePixel = 0
        TabButtonIndicator.Size = UDim2.new(0, 2, 0, 25)

        local Tab = Instance.new("ScrollingFrame")
        local TabLayout = Instance.new("UIListLayout")
        local TabPadding = Instance.new("UIPadding")

        PageClass.Page = Tab

        Tab.Name = ("%sTab"):format(PageName)
        Tab.Parent = WindowTabDisplay
        Tab.Active = true
        Tab.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
        Tab.BackgroundTransparency = 1.000
        Tab.BorderSizePixel = 0
        Tab.Position = UDim2.new(0, 0, 0.0415094346, 0)
        Tab.Size = UDim2.new(1, 0, 0.958490551, 0)
        Tab.CanvasSize = UDim2.new(0, 0, 0, 10)
        Tab.ScrollBarThickness = 0

        TabLayout.Name = "TabLayout"
        TabLayout.Parent = Tab
        TabLayout.HorizontalAlignment = Enum.HorizontalAlignment.Center
        TabLayout.SortOrder = Enum.SortOrder.LayoutOrder
        TabLayout.Padding = UDim.new(0, 35)

        TabPadding.Name = "TabPadding"
        TabPadding.Parent = Tab
        TabPadding.PaddingTop = UDim.new(0, 30)

        local ElementHandler = {}

        function ElementHandler:CreateSection(SectionName) 
            local SectionName = string.upper(SectionName) or "SECTION"
            local SectionY = 10
            local SectionOffset = 0

            local Section               = Instance.new("Frame")
            local SectionCorner         = Instance.new("UICorner")
            local ASectionText          = Instance.new("TextLabel")
            local ASectionTextPadding   = Instance.new("UIPadding")
            local SectionLayout         = Instance.new("UIListLayout")
            local SectionPadding        = Instance.new("UIPadding")

            local function UpdateSection() 
                if (Section ~= nil) then
                    Section.Size = UDim2.new(0, 478, 0, SectionY + SectionOffset)
                end
            end

            Section.Name                = "Section"
            Section.Parent              = Tab
            Section.BackgroundColor3    = Color3.fromRGB(21, 21, 21)
            Section.Position            = UDim2.new(0.0472590066, 0, 0.0590551198, 0)

            pcall(UpdateSection)

            SectionCorner.CornerRadius  = UDim.new(0, 4)
            SectionCorner.Name          = "SectionCorner"
            SectionCorner.Parent        = Section

            ASectionText.Name           = "ASectionText"
            ASectionText.Parent         = Section
            ASectionText.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
            ASectionText.BackgroundTransparency = 1.000
            ASectionText.Position       = UDim2.new(0, 0, -0.317460328, 0)
            ASectionText.Size           = UDim2.new(0, 100, 0, 22)
            ASectionText.FontFace       = Library.Font
            ASectionText.LineHeight     = 1.640
            ASectionText.Text           = SectionName
            ASectionText.TextColor3     = Color3.fromRGB(157, 157, 157)
            ASectionText.TextSize       = 17.000
            ASectionText.TextXAlignment = Enum.TextXAlignment.Left

            ASectionTextPadding.Name    = "ASectionTextPadding"
            ASectionTextPadding.Parent  = ASectionText
            ASectionTextPadding.PaddingLeft = UDim.new(0, -5)

            SectionLayout.Name          = "SectionLayout"
            SectionLayout.Parent        = Section
            SectionLayout.SortOrder     = Enum.SortOrder.LayoutOrder
            SectionLayout.Padding       = UDim.new(0, 3)

            SectionPadding.Name         = "SectionPadding"
            SectionPadding.Parent       = Section
            SectionPadding.PaddingLeft  = UDim.new(0, 5)
            SectionPadding.PaddingTop   = UDim.new(0, -20)

            local SectionHandler = {}

            function SectionHandler:CreateCheckbox(BooleanOptions) 
                local BooleanOptions = BooleanOptions or {
                    Name = BooleanOptions.Name,
                    Description = BooleanOptions.Description or "",
                    Default = BooleanOptions.Default or false,
                    Keybind = BooleanOptions.Keybind,
                    OnChanged = BooleanOptions.OnChanged or function(val) 
                        print(val)
                    end
                }

                local Manager = {}
                local Boolean_Element = Instance.new("Frame")
                local Boolean_ElementCorner = Instance.new("UICorner")
                local Boolean_ElementText = Instance.new("TextLabel")
                local Boolean_ElementCheck = Instance.new("Frame")
                local Boolean_ElementCheckCorner = Instance.new("UICorner")
                local Boolean_ElementCheckIcon = Instance.new("ImageButton")

                Boolean_Element.Name = "Boolean_Element"
                Boolean_Element.Parent = Section
                Boolean_Element.BackgroundColor3 = Color3.fromRGB(24, 24, 24)
                Boolean_Element.Position = UDim2.new(0, 0, 0.428571433, 0)
                Boolean_Element.Size = UDim2.new(1, -5, 0, 40)

                Boolean_ElementCorner.CornerRadius = UDim.new(0, 4)
                Boolean_ElementCorner.Name = "Boolean_ElementCorner"
                Boolean_ElementCorner.Parent = Boolean_Element

                Boolean_ElementText.Name = "Boolean_ElementText"
                Boolean_ElementText.Parent = Boolean_Element
                Boolean_ElementText.AnchorPoint = Vector2.new(0, 0.5)
                Boolean_ElementText.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                Boolean_ElementText.BackgroundTransparency = 1.000
                Boolean_ElementText.Position = UDim2.new(0, 15, 0.5, 0)
                Boolean_ElementText.Size = UDim2.new(0, 100, 1, 0)
                Boolean_ElementText.FontFace = Library.Font
                Boolean_ElementText.Text = BooleanOptions.Name
                Boolean_ElementText.TextColor3 = Color3.fromRGB(225, 225, 225)
                Boolean_ElementText.TextSize = 17.000
                Boolean_ElementText.TextXAlignment = Enum.TextXAlignment.Left

                Boolean_ElementCheck.Name = "Boolean_ElementCheck"
                Boolean_ElementCheck.Parent = Boolean_Element
                Boolean_ElementCheck.AnchorPoint = Vector2.new(0, 0.5)
                Boolean_ElementCheck.BackgroundColor3 = Color3.fromRGB(22, 22, 22)
                Boolean_ElementCheck.Position = UDim2.new(0.920000017, 0, 0.5, 0)
                Boolean_ElementCheck.Size = UDim2.new(0, 24, 0, 24)
                Boolean_ElementCheck.ZIndex = 1

                Boolean_ElementCheckCorner.CornerRadius = UDim.new(0, 4)
                Boolean_ElementCheckCorner.Name = "Boolean_ElementCheckCorner"
                Boolean_ElementCheckCorner.Parent = Boolean_ElementCheck

                Boolean_ElementCheckIcon.Name = "Boolean_ElementCheckIcon"
                Boolean_ElementCheckIcon.Parent = Boolean_ElementCheck
                Boolean_ElementCheckIcon.AnchorPoint = Vector2.new(0.5, 0.5)
                Boolean_ElementCheckIcon.BackgroundTransparency = 1.000
                Boolean_ElementCheckIcon.Position = UDim2.new(0.5, 0, 0.5, 0)
                Boolean_ElementCheckIcon.Size = UDim2.new(0, 20, 0, 20)
                Boolean_ElementCheckIcon.ZIndex = 2
                Boolean_ElementCheckIcon.Image = "rbxassetid://3926305904"
                Boolean_ElementCheckIcon.ImageRectOffset = Vector2.new(312, 4)
                Boolean_ElementCheckIcon.ImageRectSize = Vector2.new(24, 24)

                Boolean_ElementText.Size = UDim2.new(0, Boolean_ElementText.TextBounds.X, 0, 40)

                local Boolean_ElementCheckStroke = Support:CreateStroke(Boolean_ElementCheck, {
                    Color = Color3.fromRGB(36, 36, 36),
                    Thickness = 1,
                    Transparency = 0,
                    Enabled = true
                })

                if (BooleanOptions.Description) then 
                    local Boolean_ElementDescription = Instance.new("TextLabel")

                    Boolean_ElementDescription.Name = "Boolean_ElementDescription"
                    Boolean_ElementDescription.Parent = Boolean_Element
                    Boolean_ElementDescription.AnchorPoint = Vector2.new(0, 0.5)
                    Boolean_ElementDescription.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                    Boolean_ElementDescription.BackgroundTransparency = 1.000
                    Boolean_ElementDescription.Position = UDim2.new(0, Boolean_ElementText.Size.X.Offset + 20, 0.524999976, 0)
                    Boolean_ElementDescription.Size = UDim2.new(0.421999991, 100, 1, 0)
                    Boolean_ElementDescription.FontFace = Library.Font
                    Boolean_ElementDescription.Text = BooleanOptions.Description
                    Boolean_ElementDescription.TextColor3 = Color3.fromRGB(95, 95, 95)
                    Boolean_ElementDescription.TextSize = 13.000
                    Boolean_ElementDescription.TextWrapped = true
                    Boolean_ElementDescription.TextXAlignment = Enum.TextXAlignment.Left
                    Boolean_ElementDescription.TextTransparency = 1

                    Boolean_ElementText.MouseEnter:Connect(function()
                        Tween:Create(Boolean_ElementDescription, TweenInfo.new(.3), {
                            TextTransparency = 0
                        }):Play()
                    end)

                    Boolean_ElementText.MouseLeave:Connect(function()
                        Tween:Create(Boolean_ElementDescription, TweenInfo.new(.3), {
                            TextTransparency = 1
                        }):Play()
                    end)
                end

                local Enabled = BooleanOptions.Default
                local Debounce = false

                if (BooleanOptions.Keybind) then
                    local Boolean_ElementKeybindEdit = Instance.new("Frame")
                    local Boolean_ElementKeybindEditCorner = Instance.new("UICorner")
                    local Boolean_ElementKeybindEditButton = Instance.new("ImageButton")
                    local Boolean_ElementKeybindSelectedKey = Instance.new("TextButton")
                    local Boolean_ElementKeybindEditSeperator = Instance.new("Frame")

                    local Listening = false
                    local Editing = false
                    local KeyBind = nil

                    if (BooleanOptions.Keybind == "null" or "NULL") then 
                        KeyBind = nil
                        Boolean_ElementKeybindSelectedKey.Text = "NONE"
                    else
                        KeyBind = Enum.KeyCode[BooleanOptions.Keybind]
                        Boolean_ElementKeybindSelectedKey.Text = KeyBind.Name
                    end

                    Boolean_ElementKeybindEdit.Name = "Boolean_ElementKeybindEdit"
                    Boolean_ElementKeybindEdit.Parent = Boolean_Element
                    Boolean_ElementKeybindEdit.AnchorPoint = Vector2.new(1, 0.5)
                    Boolean_ElementKeybindEdit.BackgroundColor3 = Color3.fromRGB(18, 18, 18)
                    Boolean_ElementKeybindEdit.ClipsDescendants = true
                    Boolean_ElementKeybindEdit.Position = UDim2.new(0.89, 0, 0.5, 0)
                    Boolean_ElementKeybindEdit.Size = UDim2.new(0, 80, 0, 21)

                    Boolean_ElementKeybindEditCorner.CornerRadius = UDim.new(0, 4)
                    Boolean_ElementKeybindEditCorner.Name = "Boolean_ElementKeybindEditCorner"
                    Boolean_ElementKeybindEditCorner.Parent = Boolean_ElementKeybindEdit

                    Boolean_ElementKeybindEditButton.Name = "Boolean_ElementKeybindEditButton"
                    Boolean_ElementKeybindEditButton.Parent = Boolean_ElementKeybindEdit
                    Boolean_ElementKeybindEditButton.AnchorPoint = Vector2.new(0, 0.5)
                    Boolean_ElementKeybindEditButton.BackgroundTransparency = 1.000
                    Boolean_ElementKeybindEditButton.LayoutOrder = 4
                    Boolean_ElementKeybindEditButton.Position = UDim2.new(1, -18, 0.5, 0)
                    Boolean_ElementKeybindEditButton.Size = UDim2.new(0, 15, 0, 15)
                    Boolean_ElementKeybindEditButton.ZIndex = 2
                    Boolean_ElementKeybindEditButton.Image = "rbxassetid://3926305904"
                    Boolean_ElementKeybindEditButton.ImageColor3 = Color3.fromRGB(72, 72, 72)
                    Boolean_ElementKeybindEditButton.ImageRectOffset = Vector2.new(924, 364)
                    Boolean_ElementKeybindEditButton.ImageRectSize = Vector2.new(36, 36)

                    Boolean_ElementKeybindSelectedKey.Name = "Boolean_ElementKeybindSelectedKey"
                    Boolean_ElementKeybindSelectedKey.Parent = Boolean_ElementKeybindEdit
                    Boolean_ElementKeybindSelectedKey.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                    Boolean_ElementKeybindSelectedKey.BackgroundTransparency = 1.000
                    Boolean_ElementKeybindSelectedKey.Size = UDim2.new(0.283, 56,1, 0)
                    Boolean_ElementKeybindSelectedKey.FontFace = Library.Font
                    Boolean_ElementKeybindSelectedKey.Text = BooleanOptions.Keybind
                    Boolean_ElementKeybindSelectedKey.TextColor3 = Color3.fromRGB(83, 83, 83)
                    Boolean_ElementKeybindSelectedKey.TextSize = 14.000

                    Boolean_ElementKeybindEditSeperator.Name = "Boolean_ElementKeybindEditSeperator"
                    Boolean_ElementKeybindEditSeperator.Parent = Boolean_ElementKeybindEdit
                    Boolean_ElementKeybindEditSeperator.AnchorPoint = Vector2.new(0.5, 0.5)
                    Boolean_ElementKeybindEditSeperator.BackgroundColor3 = Color3.fromRGB(25, 25, 25)
                    Boolean_ElementKeybindEditSeperator.BorderSizePixel = 0
                    Boolean_ElementKeybindEditSeperator.Position = UDim2.new(0.75, 1, 0.5, 0)
                    Boolean_ElementKeybindEditSeperator.Size = UDim2.new(0, 1, 1, -5)

                    local function UpdateKeybindEdit() 
                        if (Editing) then 
                            Tween:Create(Boolean_ElementKeybindEdit, TweenInfo.new(.3), {
                                Size = UDim2.new(0, 120, 0, 21)
                            }):Play()
                            Tween:Create(Boolean_ElementKeybindEditSeperator, TweenInfo.new(.3), {
                                BackgroundTransparency = 0
                            }):Play()
                            Tween:Create(Boolean_ElementKeybindSelectedKey, TweenInfo.new(.3), {
                                TextTransparency = 0
                            }):Play()
                        else
                            Tween:Create(Boolean_ElementKeybindEdit, TweenInfo.new(.3), {
                                Size = UDim2.new(0, 21, 0, 21)
                            }):Play()
                            Tween:Create(Boolean_ElementKeybindEditSeperator, TweenInfo.new(.3), {
                                BackgroundTransparency = 1
                            }):Play()
                            Tween:Create(Boolean_ElementKeybindSelectedKey, TweenInfo.new(.3), {
                                TextTransparency = 1
                            }):Play()
                        end
                    end

                    pcall(UpdateKeybindEdit)
                    
                    Boolean_ElementKeybindEditButton.MouseButton1Click:Connect(function()
                        Editing = not Editing
                        UpdateKeybindEdit()
                    end)
                    
                    Library.Connections.BooleanKeybind = UIS.InputBegan:Connect(function(input)
                        if Editing and input.UserInputType == Enum.UserInputType.Keyboard then
                            if input.KeyCode == Enum.KeyCode.Escape then 
                                KeyBind = nil
                                Boolean_ElementKeybindSelectedKey.Text = "null"
                            else
                                KeyBind = input.KeyCode
                                Boolean_ElementKeybindSelectedKey.Text = KeyBind.Name
                            end

                            Editing = false
                            task.wait(.5)
                            UpdateKeybindEdit()
                        end
                    end)

                    local Debounce = false

                    local function CompleteBoolean() 
                        if (Enabled) then
                            Tween:Create(Boolean_ElementCheck, TweenInfo.new(.2), {
                                BackgroundColor3 = Color3.fromRGB(248, 72, 4)
                            }):Play()
                            Tween:Create(Boolean_ElementCheckIcon, TweenInfo.new(.2), {
                                ImageTransparency = 0
                            }):Play()
                        else
                            Tween:Create(Boolean_ElementCheck, TweenInfo.new(.2), {
                                BackgroundColor3 = Color3.fromRGB(22, 22, 22)
                            }):Play()
                            Tween:Create(Boolean_ElementCheckIcon, TweenInfo.new(.2), {
                                ImageTransparency = 1
                            }):Play()
                        end
                    end

                    Library.Connections.BooleanKeybindActivate = UIS.InputBegan:Connect(function(input)
                        if input.UserInputType == Enum.UserInputType.Keyboard then
                            if input.KeyCode == KeyBind and not Editing and not Debounce then
                                Debounce = true
                                Enabled = not Enabled
                                task.spawn(function ()
                                    local suc, req = pcall(BooleanOptions.OnChanged, Enabled)
                                    if not suc then error(req) end
                                end)

                                CompleteBoolean()
                                task.wait(0.3)
                                Debounce = false
                            end
                        end
                    end)
                end

                local function CompleteBoolean() 
                    if (Enabled) then
                        Tween:Create(Boolean_ElementCheck, TweenInfo.new(.2), {
                            BackgroundColor3 = Color3.fromRGB(248, 72, 4)
                        }):Play()
                        Tween:Create(Boolean_ElementCheckIcon, TweenInfo.new(.2), {
                            ImageTransparency = 0
                        }):Play()
                    else
                        Tween:Create(Boolean_ElementCheck, TweenInfo.new(.2), {
                            BackgroundColor3 = Color3.fromRGB(22, 22, 22)
                        }):Play()
                        Tween:Create(Boolean_ElementCheckIcon, TweenInfo.new(.2), {
                            ImageTransparency = 1
                        }):Play()
                    end
                end

                pcall(CompleteBoolean)

                Boolean_ElementCheckIcon.MouseButton1Click:Connect(function()
                    if not Debounce then
                        Debounce = true 
                        Enabled = not Enabled
                        task.spawn(function ()
                            local suc, req = pcall(BooleanOptions.OnChanged, Enabled)
                            if not suc then error(req) end
                        end)

                        CompleteBoolean()

                        task.wait(.3)
                        Debounce = false
                    end
                end)

                function Manager:IsEnabled() 
                    return Enabled
                end

                function Manager:Set(bool)
                    Enabled = bool

                    task.spawn(function ()
                        local suc, req = pcall(BooleanOptions.OnChanged, Enabled)
                        if not suc then error(req) end
                    end)

                    CompleteBoolean()
                end

                SectionOffset += 40
                UpdateSection()
                return Manager
            end

            function SectionHandler:CreateSwitch(SwitchOptions) 
                local SwitchOptions = SwitchOptions or {
                    Name = SwitchOptions.Name,
                    Description = SwitchOptions.Description or "",
                    Default = SwitchOptions.Default or false,
                    Keybind = SwitchOptions.Keybind or nil,
                    OnChanged = SwitchOptions.OnChanged or function(val) 
                        print(val)
                    end
                }
                
                local Manager = {}
                local Boolean_Element                   = Instance.new("Frame")
                local Boolean_ElementCorner             = Instance.new("UICorner")
                local Boolean_ElementText               = Instance.new("TextLabel")
                local Boolean_ElementState              = Instance.new("ImageLabel")
                local Boolean_ElementStateSelector      = Instance.new("ImageLabel")
                local Boolean_ElementButton             = Instance.new("TextButton")

                Boolean_Element.Name                    = "Boolean_Element"
                Boolean_Element.Parent                  = Section
                Boolean_Element.BackgroundColor3        = Color3.fromRGB(24, 24, 24)
                Boolean_Element.Position                = UDim2.new(0, 0, 0.428571433, 0)
                Boolean_Element.Size                    = UDim2.new(1, -5, 0, 40)

                Boolean_ElementCorner.CornerRadius      = UDim.new(0, 4)
                Boolean_ElementCorner.Name              = "Boolean_ElementCorner"
                Boolean_ElementCorner.Parent            = Boolean_Element

                Boolean_ElementText.Name                = "Boolean_ElementText"
                Boolean_ElementText.Parent              = Boolean_Element
                Boolean_ElementText.AnchorPoint         = Vector2.new(0, 0.5)
                Boolean_ElementText.BackgroundColor3    = Color3.fromRGB(255, 255, 255)
                Boolean_ElementText.BackgroundTransparency = 1.000
                Boolean_ElementText.Position            = UDim2.new(0, 15, 0.5, 0)
                Boolean_ElementText.Size                = UDim2.new(0, 100, 1, 0)
                Boolean_ElementText.FontFace            = Library.Font
                Boolean_ElementText.Text                = SwitchOptions.Name
                Boolean_ElementText.TextColor3          = Color3.fromRGB(225, 225, 225)
                Boolean_ElementText.TextSize            = 17.000
                Boolean_ElementText.TextXAlignment      = Enum.TextXAlignment.Left

                Boolean_ElementState.Name               = "Boolean_ElementState"
                Boolean_ElementState.Parent             = Boolean_Element
                Boolean_ElementState.AnchorPoint        = Vector2.new(1, 0.5)
                Boolean_ElementState.BackgroundColor3 = Color3.fromRGB(18, 18, 18)
                Boolean_ElementState.BackgroundTransparency = 1.000
                Boolean_ElementState.Position           = UDim2.new(0.970401704, 0, 0.5, 0)
                Boolean_ElementState.Size               = UDim2.new(0, 45, 0, 24)
                Boolean_ElementState.Image              = "rbxassetid://3570695787"
                Boolean_ElementState.ImageColor3      = Color3.fromRGB(18, 18, 18)
                Boolean_ElementState.ScaleType          = Enum.ScaleType.Slice
                Boolean_ElementState.SliceCenter        = Rect.new(100, 100, 100, 100)

                Boolean_ElementStateSelector.Name       = "Boolean_ElementStateSelector"
                Boolean_ElementStateSelector.Parent     = Boolean_ElementState
                Boolean_ElementStateSelector.AnchorPoint = Vector2.new(0, 0.5)
                Boolean_ElementStateSelector.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                Boolean_ElementStateSelector.BackgroundTransparency = 1.000
                Boolean_ElementStateSelector.Position   = UDim2.new(0, 2, 0.5, 0)
                Boolean_ElementStateSelector.Size       = UDim2.new(0, 20, 0, 20)
                Boolean_ElementStateSelector.Image      = "rbxassetid://3570695787"
                Boolean_ElementStateSelector.ScaleType  = Enum.ScaleType.Slice
                Boolean_ElementStateSelector.SliceCenter = Rect.new(100, 100, 100, 100)

                Boolean_ElementButton.Name              = "Boolean_ElementButton"
                Boolean_ElementButton.Parent            = Boolean_ElementState
                Boolean_ElementButton.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                Boolean_ElementButton.BackgroundTransparency = 1.000
                Boolean_ElementButton.Size              = UDim2.new(1, 0, 1, 0)
                Boolean_ElementButton.Font              = Enum.Font.SourceSans
                Boolean_ElementButton.Text              = ""
                Boolean_ElementButton.TextColor3      = Color3.fromRGB(0, 0, 0)
                Boolean_ElementButton.TextSize          = 14.000

                Boolean_ElementText.Size = UDim2.new(0, Boolean_ElementText.TextBounds.X, 0, 40)

                if (SwitchOptions.Description) then 
                    local Boolean_ElementDescription = Instance.new("TextLabel")

                    Boolean_ElementDescription.Name = "Boolean_ElementDescription"
                    Boolean_ElementDescription.Parent = Boolean_Element
                    Boolean_ElementDescription.AnchorPoint = Vector2.new(0, 0.5)
                    Boolean_ElementDescription.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                    Boolean_ElementDescription.BackgroundTransparency = 1.000
                    Boolean_ElementDescription.Position = UDim2.new(0, Boolean_ElementText.Size.X.Offset + 20, 0.524999976, 0)
                    Boolean_ElementDescription.Size = UDim2.new(0.421999991, 100, 1, 0)
                    Boolean_ElementDescription.FontFace = Library.Font
                    Boolean_ElementDescription.Text = SwitchOptions.Description
                    Boolean_ElementDescription.TextColor3 = Color3.fromRGB(95, 95, 95)
                    Boolean_ElementDescription.TextSize = 13.000
                    Boolean_ElementDescription.TextWrapped = true
                    Boolean_ElementDescription.TextXAlignment = Enum.TextXAlignment.Left
                    Boolean_ElementDescription.TextTransparency = 1

                    Boolean_ElementText.MouseEnter:Connect(function()
                        Tween:Create(Boolean_ElementDescription, TweenInfo.new(.3), {
                            TextTransparency = 0
                        }):Play()
                    end)

                    Boolean_ElementText.MouseLeave:Connect(function()
                        Tween:Create(Boolean_ElementDescription, TweenInfo.new(.3), {
                            TextTransparency = 1
                        }):Play()
                    end)
                end

                local Enabled = SwitchOptions.Default

                if (SwitchOptions.Keybind) then 
                    if not (Enum.KeyCode[SwitchOptions.Keybind]) then 
                        return
                    end

                    local Boolean_ElementKeybindEdit = Instance.new("Frame")
                    local Boolean_ElementKeybindEditCorner = Instance.new("UICorner")
                    local Boolean_ElementKeybindEditButton = Instance.new("ImageButton")
                    local Boolean_ElementKeybindSelectedKey = Instance.new("TextButton")
                    local Boolean_ElementKeybindEditSeperator = Instance.new("Frame")

                    local Listening = false
                    local Editing = false
                    local KeyBind = nil

                    if (SwitchOptions.Keybind == "null" or "NULL") then 
                        KeyBind = nil
                        Boolean_ElementKeybindSelectedKey.Text = "NONE"
                    else
                        KeyBind = Enum.KeyCode[SwitchOptions.Keybind]
                        Boolean_ElementKeybindSelectedKey.Text = KeyBind.Name
                    end

                    Boolean_ElementKeybindEdit.Name = "Boolean_ElementKeybindEdit"
                    Boolean_ElementKeybindEdit.Parent = Boolean_Element
                    Boolean_ElementKeybindEdit.AnchorPoint = Vector2.new(1, 0.5)
                    Boolean_ElementKeybindEdit.BackgroundColor3 = Color3.fromRGB(18, 18, 18)
                    Boolean_ElementKeybindEdit.ClipsDescendants = true
                    Boolean_ElementKeybindEdit.Position = UDim2.new(0.85, 0, 0.5, 0)
                    Boolean_ElementKeybindEdit.Size = UDim2.new(0, 80, 0, 21)

                    Boolean_ElementKeybindEditCorner.CornerRadius = UDim.new(0, 4)
                    Boolean_ElementKeybindEditCorner.Name = "Boolean_ElementKeybindEditCorner"
                    Boolean_ElementKeybindEditCorner.Parent = Boolean_ElementKeybindEdit

                    Boolean_ElementKeybindEditButton.Name = "Boolean_ElementKeybindEditButton"
                    Boolean_ElementKeybindEditButton.Parent = Boolean_ElementKeybindEdit
                    Boolean_ElementKeybindEditButton.AnchorPoint = Vector2.new(0, 0.5)
                    Boolean_ElementKeybindEditButton.BackgroundTransparency = 1.000
                    Boolean_ElementKeybindEditButton.LayoutOrder = 4
                    Boolean_ElementKeybindEditButton.Position = UDim2.new(1, -18, 0.5, 0)
                    Boolean_ElementKeybindEditButton.Size = UDim2.new(0, 15, 0, 15)
                    Boolean_ElementKeybindEditButton.ZIndex = 2
                    Boolean_ElementKeybindEditButton.Image = "rbxassetid://3926305904"
                    Boolean_ElementKeybindEditButton.ImageColor3 = Color3.fromRGB(72, 72, 72)
                    Boolean_ElementKeybindEditButton.ImageRectOffset = Vector2.new(924, 364)
                    Boolean_ElementKeybindEditButton.ImageRectSize = Vector2.new(36, 36)

                    Boolean_ElementKeybindSelectedKey.Name = "Boolean_ElementKeybindSelectedKey"
                    Boolean_ElementKeybindSelectedKey.Parent = Boolean_ElementKeybindEdit
                    Boolean_ElementKeybindSelectedKey.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                    Boolean_ElementKeybindSelectedKey.BackgroundTransparency = 1.000
                    Boolean_ElementKeybindSelectedKey.Size = UDim2.new(0.283, 56,1, 0)
                    Boolean_ElementKeybindSelectedKey.FontFace = Library.Font
                    Boolean_ElementKeybindSelectedKey.Text = SwitchOptions.Keybind
                    Boolean_ElementKeybindSelectedKey.TextColor3 = Color3.fromRGB(83, 83, 83)
                    Boolean_ElementKeybindSelectedKey.TextSize = 14.000

                    Boolean_ElementKeybindEditSeperator.Name = "Boolean_ElementKeybindEditSeperator"
                    Boolean_ElementKeybindEditSeperator.Parent = Boolean_ElementKeybindEdit
                    Boolean_ElementKeybindEditSeperator.AnchorPoint = Vector2.new(0.5, 0.5)
                    Boolean_ElementKeybindEditSeperator.BackgroundColor3 = Color3.fromRGB(25, 25, 25)
                    Boolean_ElementKeybindEditSeperator.BorderSizePixel = 0
                    Boolean_ElementKeybindEditSeperator.Position = UDim2.new(0.75, 1, 0.5, 0)
                    Boolean_ElementKeybindEditSeperator.Size = UDim2.new(0, 1, 1, -5)

                    local function UpdateKeybindEdit() 
                        if (Editing) then 
                            Tween:Create(Boolean_ElementKeybindEdit, TweenInfo.new(.3), {
                                Size = UDim2.new(0, 120, 0, 21)
                            }):Play()
                            Tween:Create(Boolean_ElementKeybindEditSeperator, TweenInfo.new(.3), {
                                BackgroundTransparency = 0
                            }):Play()
                            Tween:Create(Boolean_ElementKeybindSelectedKey, TweenInfo.new(.3), {
                                TextTransparency = 0
                            }):Play()
                        else
                            Tween:Create(Boolean_ElementKeybindEdit, TweenInfo.new(.3), {
                                Size = UDim2.new(0, 21, 0, 21)
                            }):Play()
                            Tween:Create(Boolean_ElementKeybindEditSeperator, TweenInfo.new(.3), {
                                BackgroundTransparency = 1
                            }):Play()
                            Tween:Create(Boolean_ElementKeybindSelectedKey, TweenInfo.new(.3), {
                                TextTransparency = 1
                            }):Play()
                        end
                    end

                    pcall(UpdateKeybindEdit)
                    
                    Boolean_ElementKeybindEditButton.MouseButton1Click:Connect(function()
                        Editing = not Editing
                        UpdateKeybindEdit()
                    end)
                    
                    Library.Connections.BooleanKeybind = UIS.InputBegan:Connect(function(input)
                        if Editing and input.UserInputType == Enum.UserInputType.Keyboard then
                            if input.KeyCode == Enum.KeyCode.Escape then 
                                KeyBind = nil
                                Boolean_ElementKeybindSelectedKey.Text = "null"
                            else
                                KeyBind = input.KeyCode
                                Boolean_ElementKeybindSelectedKey.Text = KeyBind.Name
                            end

                            Editing = false
                            task.wait(.5)
                            UpdateKeybindEdit()
                        end
                    end)

                    local Debounce = false

                    local function CompleteBoolean() 
                        if (Enabled) then
                            Tween:Create(Boolean_ElementState, TweenInfo.new(.2), {
                                ImageColor3 = Color3.fromRGB(248, 72, 4)
                            }):Play()
                            Tween:Create(Boolean_ElementStateSelector, TweenInfo.new(.2), {
                                Position = UDim2.new(0, 22, 0.5, 0)
                            }):Play()
                        else
                            Tween:Create(Boolean_ElementState, TweenInfo.new(.2), {
                                ImageColor3 = Color3.fromRGB(18, 18, 18)
                            }):Play()
                            Tween:Create(Boolean_ElementStateSelector, TweenInfo.new(.2), {
                                Position = UDim2.new(0, 2, 0.5, 0)
                            }):Play()
                        end
                    end

                    Library.Connections.BooleanKeybindActivate = UIS.InputBegan:Connect(function(input)
                        if input.UserInputType == Enum.UserInputType.Keyboard then
                            if input.KeyCode == KeyBind and not Editing and not Debounce then
                                Debounce = true
                                Enabled = not Enabled
                                task.spawn(function ()
                                    local suc, req = pcall(SwitchOptions.OnChanged, Enabled)
                                    if not suc then error(req) end
                                end)
                                CompleteBoolean()
                                task.wait(0.3)
                                Debounce = false
                            end
                        end
                    end)
                end

                local Debounce = false

                local function CompleteBoolean() 
                    if (Enabled) then
                        Tween:Create(Boolean_ElementState, TweenInfo.new(.2), {
                            ImageColor3 = Color3.fromRGB(248, 72, 4)
                        }):Play()
                        Tween:Create(Boolean_ElementStateSelector, TweenInfo.new(.2), {
                            Position = UDim2.new(0, 22, 0.5, 0)
                        }):Play()
                    else
                        Tween:Create(Boolean_ElementState, TweenInfo.new(.2), {
                            ImageColor3 = Color3.fromRGB(18, 18, 18)
                        }):Play()
                        Tween:Create(Boolean_ElementStateSelector, TweenInfo.new(.2), {
                            Position = UDim2.new(0, 2, 0.5, 0)
                        }):Play()
                    end
                end

                pcall(CompleteBoolean)

                Boolean_ElementButton.MouseButton1Click:Connect(function()
                    if not Debounce then
                        Debounce = true 
                        Enabled = not Enabled
                        task.spawn(function ()
                            local suc, req = pcall(SwitchOptions.OnChanged, Enabled)
                            if not suc then error(req) end
                        end)

                        CompleteBoolean()

                        task.wait(.3)
                        Debounce = false
                    end
                end)

                function Manager:IsEnabled() 
                    return Enabled
                end

                function Manager:Set(bool)
                    Enabled = bool

                    task.spawn(function ()
                        local suc, req = pcall(SwitchOptions.OnChanged, Enabled)
                        if not suc then error(req) end
                    end)

                    CompleteBoolean()
                end

                SectionOffset += 40
                UpdateSection()
                return Manager
            end 

            function SectionHandler:CreateSlider(SliderOptions) 
                local SliderOptions = SliderOptions or {
                    Name = SliderOptions.Name,
                    Description = SliderOptions.Description or "",
                    Keybind = SliderOptions.Slider,
                    Range = SliderOptions.Range or { 10, 100 },
                    Default = SliderOptions.Default or { 60 },
                    OnChanged = SliderOptions.OnChanged or function(value) 
                        print(value)
                    end
                }

                local Manager = {}
                local Slider_Element                    = Instance.new("Frame")
                local Slider_ElementText                = Instance.new("TextLabel")
                local Slider_ElementCorner              = Instance.new("UICorner")
                local Slider_ElementActivator           = Instance.new("ImageLabel")
                local Slider_ElementActivatorFill       = Instance.new("Frame")
                local Slider_ElementActivatorFillCorner = Instance.new("UICorner")
                local Slider_ElementActivatorButton     = Instance.new("TextButton")
                local Slider_ElementValue               = Instance.new("Frame")
                local Slider_ElementValueCorner         = Instance.new("UICorner")
                local Slider_ElementValueDisplay        = Instance.new("TextBox")

                Slider_Element.MouseEnter:Connect(function()
                    Tween:Create(Slider_Element, TweenInfo.new(.3), {
                        BackgroundColor3 = Color3.fromRGB(27, 27, 27)
                    }):Play()
                end)

                Slider_Element.MouseLeave:Connect(function()
                    Tween:Create(Slider_Element, TweenInfo.new(.3), {
                        BackgroundColor3 = Color3.fromRGB(24, 24, 24)
                    }):Play()
                end)

                Slider_Element.Name                     = "Slider_Element"
                Slider_Element.Parent                   = Section
                Slider_Element.BackgroundColor3         = Color3.fromRGB(24, 24, 24)
                Slider_Element.Position                 = UDim2.new(-0.00638443418, 0, 0.674074054, 0)
                Slider_Element.Size                     = UDim2.new(1, -5, 0, 60)
                
                Slider_ElementText.Name                 = "Slider_ElementText"
                Slider_ElementText.Parent               = Slider_Element
                Slider_ElementText.AnchorPoint          = Vector2.new(0, 0.5)
                Slider_ElementText.BackgroundColor3     = Color3.fromRGB(255, 255, 255)
                Slider_ElementText.BackgroundTransparency = 1.000
                Slider_ElementText.Position             = UDim2.new(0, 15, 0.36153847, 0)
                Slider_ElementText.Size                 = UDim2.new(0, 100, 0, 40)
                Slider_ElementText.FontFace             = Library.Font
                Slider_ElementText.Text                 = SliderOptions.Name
                Slider_ElementText.TextColor3           = Color3.fromRGB(225, 225, 225)
                Slider_ElementText.TextSize             = 17.000
                Slider_ElementText.TextXAlignment       = Enum.TextXAlignment.Left
                
                Slider_ElementCorner.CornerRadius       = UDim.new(0, 4)
                Slider_ElementCorner.Name               = "Slider_ElementCorner"
                Slider_ElementCorner.Parent             = Slider_Element
                
                Slider_ElementActivator.Name            = "Slider_ElementActivator"
                Slider_ElementActivator.Parent          = Slider_Element
                Slider_ElementActivator.Active          = true
                Slider_ElementActivator.AnchorPoint     = Vector2.new(0.5, 0.5)
                Slider_ElementActivator.BackgroundColor3 = Color3.fromRGB(18, 18, 18)
                Slider_ElementActivator.BackgroundTransparency = 1.000
                Slider_ElementActivator.Position        = UDim2.new(0.431289643, 0, 0.730769157, 0)
                Slider_ElementActivator.Selectable      = true
                Slider_ElementActivator.Size            = UDim2.new(0.803382635, 0, 0.150000006, 0)
                Slider_ElementActivator.Image           = "rbxassetid://3570695787"
                Slider_ElementActivator.ImageColor3     = Color3.fromRGB(18, 18, 18)
                Slider_ElementActivator.ScaleType       = Enum.ScaleType.Slice
                Slider_ElementActivator.SliceCenter     = Rect.new(100, 100, 100, 100)
                
                Slider_ElementActivatorFill.Name        = "Slider_ElementActivatorFill"
                Slider_ElementActivatorFill.Parent      = Slider_ElementActivator
                Slider_ElementActivatorFill.AnchorPoint = Vector2.new(0, 0.5)
                Slider_ElementActivatorFill.BackgroundColor3 = Color3.fromRGB(248, 72, 4)
                Slider_ElementActivatorFill.Position    = UDim2.new(0, 0, 0.5, 0)
                Slider_ElementActivatorFill.Size        = UDim2.new(0, 100, 1, -2)
                Slider_ElementActivatorFill.BorderSizePixel = 0
                
                Slider_ElementActivatorFillCorner.CornerRadius = UDim.new(0, 4)
                Slider_ElementActivatorFillCorner.Name  = "Slider_ElementActivatorFillCorner"
                Slider_ElementActivatorFillCorner.Parent = Slider_ElementActivatorFill
                
                Slider_ElementActivatorButton.Name      = "Slider_ElementActivatorButton"
                Slider_ElementActivatorButton.Parent    = Slider_ElementActivator
                Slider_ElementActivatorButton.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                Slider_ElementActivatorButton.BackgroundTransparency = 1.000
                Slider_ElementActivatorButton.Size      = UDim2.new(1, 0, 1, 0)
                Slider_ElementActivatorButton.Font      = Enum.Font.SourceSans
                Slider_ElementActivatorButton.Text      = ""
                Slider_ElementActivatorButton.TextColor3 = Color3.fromRGB(0, 0, 0)
                Slider_ElementActivatorButton.TextSize  = 14.000
                
                Slider_ElementValue.Name                = "Slider_ElementValue"
                Slider_ElementValue.Parent              = Slider_Element
                Slider_ElementValue.BackgroundColor3    = Color3.fromRGB(18, 18, 18)
                Slider_ElementValue.ClipsDescendants    = true
                Slider_ElementValue.Position            = UDim2.new(0.856236815, 0, 0.552380919, 0)
                Slider_ElementValue.Size                = UDim2.new(0, 63, 0, 20)
                
                Slider_ElementValueCorner.CornerRadius  = UDim.new(0, 4)
                Slider_ElementValueCorner.Name          = "Slider_ElementValueCorner"
                Slider_ElementValueCorner.Parent        = Slider_ElementValue
                
                Slider_ElementValueDisplay.Name         = "Slider_ElementValueDisplay"
                Slider_ElementValueDisplay.Parent       = Slider_ElementValue
                Slider_ElementValueDisplay.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                Slider_ElementValueDisplay.BackgroundTransparency = 1.000
                Slider_ElementValueDisplay.Size         = UDim2.new(1, 0, 1, 0)
                Slider_ElementValueDisplay.ClearTextOnFocus = false
                Slider_ElementValueDisplay.FontFace     = Library.Font
                Slider_ElementValueDisplay.PlaceholderColor3 = Color3.fromRGB(240, 240, 240)
                Slider_ElementValueDisplay.Text         = SliderOptions.Default[1]
                Slider_ElementValueDisplay.TextColor3   = Color3.fromRGB(240, 240, 240)
                Slider_ElementValueDisplay.TextSize     = 14.000

                Slider_ElementText.Size = UDim2.new(0, Slider_ElementText.TextBounds.X, 0, 40)

                if (SliderOptions.Description) then 
                    local Slider_ElementDescription = Instance.new("TextLabel")

                    Slider_ElementDescription.Name = "Slider_ElementDescription"
                    Slider_ElementDescription.Parent = Slider_Element
                    Slider_ElementDescription.AnchorPoint = Vector2.new(0, 0.5)
                    Slider_ElementDescription.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                    Slider_ElementDescription.BackgroundTransparency = 1.000
                    Slider_ElementDescription.Position = UDim2.new(0, Slider_ElementText.Size.X.Offset + 20, 0.352544785, 0)
                    Slider_ElementDescription.Size = UDim2.new(0.58759135, 100, 0.53093797, 0)
                    Slider_ElementDescription.FontFace = Library.Font
                    Slider_ElementDescription.LineHeight = 0.820
                    Slider_ElementDescription.Text = SliderOptions.Description
                    Slider_ElementDescription.TextColor3 = Color3.fromRGB(95, 95, 95)
                    Slider_ElementDescription.TextSize = 13.000
                    Slider_ElementDescription.TextWrapped = true
                    Slider_ElementDescription.TextXAlignment = Enum.TextXAlignment.Left
                    Slider_ElementDescription.TextTransparency = 1

                    Slider_ElementText.MouseEnter:Connect(function()
                        Tween:Create(Slider_ElementDescription, TweenInfo.new(.3), {
                            TextTransparency = 0
                        }):Play()
                    end)

                    Slider_ElementText.MouseLeave:Connect(function()
                        Tween:Create(Slider_ElementDescription, TweenInfo.new(.3), {
                            TextTransparency = 1
                        }):Play()
                    end)
                end

                local MouseLoc = UIS:GetMouseLocation()
                local Fill, Frame, Button = Slider_ElementActivatorFill, Slider_ElementActivator, Slider_ElementActivatorButton
                local Min, Max = SliderOptions.Range[1], SliderOptions.Range[2]
                local Default = SliderOptions.Default[1]
                local Value = Default
                local Dragging = false
                local SliderEnabled = true

                local function UpdateSliderToggle() 
                    if SliderEnabled then 
                        Tween:Create(Slider_ElementActivatorFill, TweenInfo.new(.3), {
                            BackgroundColor3 = Color3.fromRGB(248, 72, 4)
                        }):Play()
                    else
                        Tween:Create(Slider_ElementActivatorFill, TweenInfo.new(.3), {
                            BackgroundColor3 = Color3.fromRGB(61, 61, 61)
                        }):Play()
                    end
                end

                if (SliderOptions.Keybind) then
                    local Boolean_ElementKeybindEdit = Instance.new("Frame")
                    local Boolean_ElementKeybindEditCorner = Instance.new("UICorner")
                    local Boolean_ElementKeybindEditButton = Instance.new("ImageButton")
                    local Boolean_ElementKeybindSelectedKey = Instance.new("TextButton")
                    local Boolean_ElementKeybindEditSeperator = Instance.new("Frame")

                    local Listening = false
                    local Editing = false
                    local KeyBind = nil

                    if (SliderOptions.Keybind == "null" or "NULL") then 
                        KeyBind = nil
                        Boolean_ElementKeybindSelectedKey.Text = "NONE"
                    else
                        KeyBind = Enum.KeyCode[SliderOptions.Keybind]
                        Boolean_ElementKeybindSelectedKey.Text = KeyBind.Name
                    end

                    Boolean_ElementKeybindEdit.Name = "Boolean_ElementKeybindEdit"
                    Boolean_ElementKeybindEdit.Parent = Slider_Element
                    Boolean_ElementKeybindEdit.AnchorPoint = Vector2.new(1, 0.5)
                    Boolean_ElementKeybindEdit.BackgroundColor3 = Color3.fromRGB(18, 18, 18)
                    Boolean_ElementKeybindEdit.ClipsDescendants = true
                    Boolean_ElementKeybindEdit.Position = UDim2.new(1, -4, 0.5, -10)
                    Boolean_ElementKeybindEdit.Size = UDim2.new(0, 80, 0, 21)

                    Boolean_ElementKeybindEditCorner.CornerRadius = UDim.new(0, 4)
                    Boolean_ElementKeybindEditCorner.Name = "Boolean_ElementKeybindEditCorner"
                    Boolean_ElementKeybindEditCorner.Parent = Boolean_ElementKeybindEdit

                    Boolean_ElementKeybindEditButton.Name = "Boolean_ElementKeybindEditButton"
                    Boolean_ElementKeybindEditButton.Parent = Boolean_ElementKeybindEdit
                    Boolean_ElementKeybindEditButton.AnchorPoint = Vector2.new(0, 0.5)
                    Boolean_ElementKeybindEditButton.BackgroundTransparency = 1.000
                    Boolean_ElementKeybindEditButton.LayoutOrder = 4
                    Boolean_ElementKeybindEditButton.Position = UDim2.new(1, -18, 0.5, 0)
                    Boolean_ElementKeybindEditButton.Size = UDim2.new(0, 15, 0, 15)
                    Boolean_ElementKeybindEditButton.ZIndex = 2
                    Boolean_ElementKeybindEditButton.Image = "rbxassetid://3926305904"
                    Boolean_ElementKeybindEditButton.ImageColor3 = Color3.fromRGB(72, 72, 72)
                    Boolean_ElementKeybindEditButton.ImageRectOffset = Vector2.new(924, 364)
                    Boolean_ElementKeybindEditButton.ImageRectSize = Vector2.new(36, 36)

                    Boolean_ElementKeybindSelectedKey.Name = "Boolean_ElementKeybindSelectedKey"
                    Boolean_ElementKeybindSelectedKey.Parent = Boolean_ElementKeybindEdit
                    Boolean_ElementKeybindSelectedKey.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                    Boolean_ElementKeybindSelectedKey.BackgroundTransparency = 1.000
                    Boolean_ElementKeybindSelectedKey.Size = UDim2.new(0.283, 56,1, 0)
                    Boolean_ElementKeybindSelectedKey.FontFace = Library.Font
                    Boolean_ElementKeybindSelectedKey.Text = SliderOptions.Keybind
                    Boolean_ElementKeybindSelectedKey.TextColor3 = Color3.fromRGB(83, 83, 83)
                    Boolean_ElementKeybindSelectedKey.TextSize = 14.000

                    Boolean_ElementKeybindEditSeperator.Name = "Boolean_ElementKeybindEditSeperator"
                    Boolean_ElementKeybindEditSeperator.Parent = Boolean_ElementKeybindEdit
                    Boolean_ElementKeybindEditSeperator.AnchorPoint = Vector2.new(0.5, 0.5)
                    Boolean_ElementKeybindEditSeperator.BackgroundColor3 = Color3.fromRGB(25, 25, 25)
                    Boolean_ElementKeybindEditSeperator.BorderSizePixel = 0
                    Boolean_ElementKeybindEditSeperator.Position = UDim2.new(0.75, 1, 0.5, 0)
                    Boolean_ElementKeybindEditSeperator.Size = UDim2.new(0, 1, 1, -5)

                    local function UpdateKeybindEdit() 
                        if (Editing) then 
                            Tween:Create(Boolean_ElementKeybindEdit, TweenInfo.new(.3), {
                                Size = UDim2.new(0, 120, 0, 21)
                            }):Play()
                            Tween:Create(Boolean_ElementKeybindEditSeperator, TweenInfo.new(.3), {
                                BackgroundTransparency = 0
                            }):Play()
                            Tween:Create(Boolean_ElementKeybindSelectedKey, TweenInfo.new(.3), {
                                TextTransparency = 0
                            }):Play()
                        else
                            Tween:Create(Boolean_ElementKeybindEdit, TweenInfo.new(.3), {
                                Size = UDim2.new(0, 21, 0, 21)
                            }):Play()
                            Tween:Create(Boolean_ElementKeybindEditSeperator, TweenInfo.new(.3), {
                                BackgroundTransparency = 1
                            }):Play()
                            Tween:Create(Boolean_ElementKeybindSelectedKey, TweenInfo.new(.3), {
                                TextTransparency = 1
                            }):Play()
                        end
                    end

                    pcall(UpdateKeybindEdit)
                    
                    Boolean_ElementKeybindEditButton.MouseButton1Click:Connect(function()
                        Editing = not Editing
                        UpdateKeybindEdit()
                    end)
                    
                    Library.Connections.SliderKeybind = UIS.InputBegan:Connect(function(input)
                        if Editing and input.UserInputType == Enum.UserInputType.Keyboard then
                            if input.KeyCode == Enum.KeyCode.Escape then 
                                KeyBind = nil
                                Boolean_ElementKeybindSelectedKey.Text = "null"
                            else
                                KeyBind = input.KeyCode
                                Boolean_ElementKeybindSelectedKey.Text = KeyBind.Name
                            end

                            Editing = false
                            task.wait(.5)
                            UpdateKeybindEdit()
                        end
                    end)

                    local Debounce = false

                    Library.Connections.SliderKeybindActivate = UIS.InputBegan:Connect(function(input)
                        if input.UserInputType == Enum.UserInputType.Keyboard then
                            if input.KeyCode == KeyBind and not Editing then
                                SliderEnabled = not SliderEnabled
                                UpdateSliderToggle()

                                local suc, req = pcall(SliderOptions.OnChanged, Value, SliderEnabled)
                                if not suc then
                                    error(req)
                                end
                                
                            end
                        end
                    end)
                end

                if (Default < Min) then
                    Default = Min
                elseif (Default > Max) then
                    Default = Max
                end

                local function UpdateValueDisplay() 
                    Slider_ElementValueDisplay.Text = Value
                end

                local function UpdateFillSize() 
                    --> Should Update the X size of the slider fill
                    local range = Max - Min
                    local percent = (Value - Min) / range
                    local fillPercent = math.clamp(percent, 0, 1)
                    local fillSize = UDim2.new(math.max(fillPercent, 0.03), 0, 1, 0)
                    Tween:Create(Fill, TweenInfo.new(.3), { Size = fillSize }):Play()
                end
                
                local function UpdateValue(n) 
                    Value = math.clamp(n, Min, Max)
                    pcall(UpdateValueDisplay)
                    pcall(UpdateFillSize)
                    local suc, req = pcall(SliderOptions.OnChanged, Value, SliderEnabled)
                    if not suc then
                        error(req)
                    end
                end
                
                pcall(UpdateValue, Value, SliderEnabled)
                
                Library.Connections.SliderBegin = Slider_ElementActivatorButton.MouseButton1Down:Connect(function()
                    Dragging = true
                end)
                
                Library.Connections.SliderChanged = UIS.InputChanged:Connect(function(inputObject)
                    if Dragging and SliderEnabled and inputObject.UserInputType == Enum.UserInputType.MouseMovement then
                        local inputPosition = inputObject.Position
                        local framePosition = Frame.AbsolutePosition
                        local frameWidth = Frame.AbsoluteSize.X
                        local percent = math.clamp((inputPosition.X - framePosition.X) / frameWidth, 0, 1)
                        Value = Min + math.floor(percent * (Max - Min) + 0.5)
                        UpdateValue(Value)
                    end
                end)
                
                Library.Connections.SliderEnded = UIS.InputEnded:Connect(function(inputObject)
                    if inputObject.UserInputType == Enum.UserInputType.MouseButton1 then
                        Dragging = false
                    end
                end)

                Library.Connections.SliderSetValue = Slider_ElementValueDisplay:GetPropertyChangedSignal("Text"):Connect(function()
                    Value = tonumber(Slider_ElementValueDisplay.Text)
                    pcall(UpdateValue, Value, SliderEnabled)
                end)

                Library.Connections.SliderCompleteValue = Slider_ElementValueDisplay.FocusLost:Connect(function(enterPressed, inputThatCausedFocusLoss)
                    Value = tonumber(Slider_ElementValueDisplay.Text)
                    pcall(UpdateValue, Value, SliderEnabled)
                end) 

                function Manager:GetValue() 
                    return Value
                end 

                function Manager:Set(n) 
                    local suc, req = pcall(UpdateValue, n, SliderEnabled)
                    if not suc then
                        error(req)
                    end
                end 

                SectionOffset += 63
                UpdateSection()
                return Manager
            end

            function SectionHandler:CreateDropdown(DropdownOptions) 
                local DropdownOptions = DropdownOptions or {
                    Name = DropdownOptions.Name,
                    Description = DropdownOptions.Description or "",
                    Items = DropdownOptions.Items or { "1", "2", "3" },
                    Default = DropdownOptions.Default or { "1" },
                    OnSelected = DropdownOptions.OnSelected or function (value)
                        print(value)
                    end
                }

                local Default = DropdownOptions.Default

                if (DropdownOptions.Items[Default]) then
                    Default = Default
                else
                    Default = DropdownOptions.Items[1]
                end

                local Dropdown_Element              = Instance.new("Frame")
                local Dropdown_ElementCorner        = Instance.new("UICorner")
                local Dropdown_ElementText          = Instance.new("TextLabel")
                local Dropdown_ElementContainer     = Instance.new("Frame")
                local Dropdown_ElementContainerCorner   = Instance.new("UICorner")
                local Dropdown_ElementSelectedOption    = Instance.new("TextLabel")
                local Dropdown_ElementSelectedOptionPadding = Instance.new("UIPadding")
                local Dropdown_ElementExpandStateIcon   = Instance.new("ImageButton")
                local Dropdown_ElementButton            = Instance.new("TextButton")
                local Dropdown_ElementContainerLayout   = Instance.new("UIListLayout")
                local Dropdown_ElementContainerOptions  = Instance.new("ScrollingFrame")
                local Dropdown_ElementContainerOptionsLayout = Instance.new("UIListLayout")
                local Dropdown_ElementContainerOptionsPadding = Instance.new("UIPadding")

                Dropdown_Element.MouseEnter:Connect(function()
                    Tween:Create(Dropdown_Element, TweenInfo.new(.3), {
                        BackgroundColor3 = Color3.fromRGB(27, 27, 27)
                    }):Play()
                end)

                Dropdown_Element.MouseLeave:Connect(function()
                    Tween:Create(Dropdown_Element, TweenInfo.new(.3), {
                        BackgroundColor3 = Color3.fromRGB(24, 24, 24)
                    }):Play()
                end)

                Dropdown_ElementContainerOptionsPadding.Name = "Dropdown_ElementContainerOptionsPadding"
                Dropdown_ElementContainerOptionsPadding.Parent = Dropdown_ElementContainerOptions
                Dropdown_ElementContainerOptionsPadding.PaddingBottom = UDim.new(0, 3)
                Dropdown_ElementContainerOptionsPadding.PaddingTop = UDim.new(0, 3)
                
                Dropdown_Element.Name               = "Dropdown_Element"
                Dropdown_Element.Parent             = Section
                Dropdown_Element.BackgroundColor3   = Color3.fromRGB(24, 24, 24)
                Dropdown_Element.Position           = UDim2.new(0.010438445, 0, 0.462500006, 0)
                Dropdown_Element.Size               = UDim2.new(1, -5, 0, 73)

                Dropdown_ElementCorner.CornerRadius = UDim.new(0, 4)
                Dropdown_ElementCorner.Name         = "Dropdown_ElementCorner"
                Dropdown_ElementCorner.Parent       = Dropdown_Element

                Dropdown_ElementText.Name           = "Dropdown_ElementText"
                Dropdown_ElementText.Parent         = Dropdown_Element
                Dropdown_ElementText.AnchorPoint    = Vector2.new(0, 0.5)
                Dropdown_ElementText.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                Dropdown_ElementText.BackgroundTransparency = 1.000
                Dropdown_ElementText.Position       = UDim2.new(0, 15, 0, 20)
                Dropdown_ElementText.Size           = UDim2.new(0, 100, 0, 40)
                Dropdown_ElementText.FontFace       = Library.Font
                Dropdown_ElementText.Text           = DropdownOptions.Name
                Dropdown_ElementText.TextColor3     = Color3.fromRGB(225, 225, 225)
                Dropdown_ElementText.TextSize       = 17.000
                Dropdown_ElementText.TextXAlignment = Enum.TextXAlignment.Left

                Dropdown_ElementContainer.Name      = "Dropdown_ElementContainer"
                Dropdown_ElementContainer.Parent    = Dropdown_Element
                Dropdown_ElementContainer.BackgroundColor3 = Color3.fromRGB(18, 18, 18)
                Dropdown_ElementContainer.ClipsDescendants = true
                Dropdown_ElementContainer.Position  = UDim2.new(0.0253745522, 0, 0, 35)
                Dropdown_ElementContainer.Size      = UDim2.new(0, 439, 0, 27)

                local Dropdown_Stroke               = Support:CreateStroke(Dropdown_ElementContainer, {
                    Color = Color3.fromRGB(30, 30, 30),
                    Thickness = 1,
                    Transparency = 0,
                    Enabled = true
                })

                Dropdown_ElementContainerCorner.CornerRadius = UDim.new(0, 4)
                Dropdown_ElementContainerCorner.Name         = "Dropdown_ElementContainerCorner"
                Dropdown_ElementContainerCorner.Parent       = Dropdown_ElementContainer

                Dropdown_ElementSelectedOption.Name          = "Dropdown_ElementSelectedOption"
                Dropdown_ElementSelectedOption.Parent        = Dropdown_ElementContainer
                Dropdown_ElementSelectedOption.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                Dropdown_ElementSelectedOption.BackgroundTransparency = 1.000
                Dropdown_ElementSelectedOption.Position      = UDim2.new(3.43666429e-08, 0, 0, 0)
                Dropdown_ElementSelectedOption.Size          = UDim2.new(1, 0, 0, 24)
                Dropdown_ElementSelectedOption.FontFace      = Library.Font
                Dropdown_ElementSelectedOption.Text          = Default
                Dropdown_ElementSelectedOption.TextColor3    = Color3.fromRGB(206, 206, 206)
                Dropdown_ElementSelectedOption.TextSize      = 14.000
                Dropdown_ElementSelectedOption.TextXAlignment= Enum.TextXAlignment.Left

                Dropdown_ElementSelectedOptionPadding.Name          = "Dropdown_ElementSelectedOptionPadding"
                Dropdown_ElementSelectedOptionPadding.Parent        = Dropdown_ElementSelectedOption
                Dropdown_ElementSelectedOptionPadding.PaddingLeft   = UDim.new(0, 10)
                Dropdown_ElementSelectedOptionPadding.PaddingTop    = UDim.new(0, 2)

                Dropdown_ElementExpandStateIcon.Name                = "Dropdown_ElementExpandStateIcon"
                Dropdown_ElementExpandStateIcon.Parent              = Dropdown_ElementSelectedOption
                Dropdown_ElementExpandStateIcon.BackgroundTransparency = 1.000
                Dropdown_ElementExpandStateIcon.Position            = UDim2.new(0.936936975, 0, 0, -2)
                Dropdown_ElementExpandStateIcon.Size                = UDim2.new(0, 25, 0, 25)
                Dropdown_ElementExpandStateIcon.ZIndex              = 2
                Dropdown_ElementExpandStateIcon.Image               = "rbxassetid://3926305904"
                Dropdown_ElementExpandStateIcon.ImageColor3         = Color3.fromRGB(184, 184, 184)
                Dropdown_ElementExpandStateIcon.ImageRectOffset     = Vector2.new(564, 284)
                Dropdown_ElementExpandStateIcon.ImageRectSize       = Vector2.new(36, 36)

                Dropdown_ElementButton.Name                         = "Dropdown_ElementButton"
                Dropdown_ElementButton.Parent                       = Dropdown_ElementSelectedOption
                Dropdown_ElementButton.BackgroundColor3             = Color3.fromRGB(255, 255, 255)
                Dropdown_ElementButton.BackgroundTransparency       = 1.000
                Dropdown_ElementButton.Position                     = UDim2.new(-0.025, 0,-0.068, 0)
                Dropdown_ElementButton.Size                         = UDim2.new(1,0,1,0)
                Dropdown_ElementButton.Font                         = Enum.Font.SourceSans
                Dropdown_ElementButton.Text                         = ""
                Dropdown_ElementButton.TextColor3                   = Color3.fromRGB(0, 0, 0)
                Dropdown_ElementButton.TextSize                     = 14.000

                Dropdown_ElementContainerLayout.Name                = "Dropdown_ElementContainerLayout"
                Dropdown_ElementContainerLayout.Parent              = Dropdown_ElementContainer
                Dropdown_ElementContainerLayout.HorizontalAlignment = Enum.HorizontalAlignment.Center
                Dropdown_ElementContainerLayout.SortOrder           = Enum.SortOrder.LayoutOrder
                Dropdown_ElementContainerLayout.Padding             = UDim.new(0, 5)

                Dropdown_ElementContainerOptions.Name               = "Dropdown_ElementContainerOptions"
                Dropdown_ElementContainerOptions.Parent             = Dropdown_ElementContainer
                Dropdown_ElementContainerOptions.Active             = true
                Dropdown_ElementContainerOptions.BackgroundColor3   = Color3.fromRGB(22, 22, 22)
                Dropdown_ElementContainerOptions.BackgroundTransparency = 1.000
                Dropdown_ElementContainerOptions.BorderSizePixel    = 0
                Dropdown_ElementContainerOptions.Position           = UDim2.new(0, 0, 0.285714298, 0)
                Dropdown_ElementContainerOptions.Size               = UDim2.new(1, 0, 0, 65)
                Dropdown_ElementContainerOptions.CanvasSize         = UDim2.new(0, 0, 0, 90)
                Dropdown_ElementContainerOptions.ScrollBarThickness = 0

                Dropdown_ElementContainerOptionsLayout.Name         = "Dropdown_ElementContainerOptionsLayout"
                Dropdown_ElementContainerOptionsLayout.Parent       = Dropdown_ElementContainerOptions
                Dropdown_ElementContainerOptionsLayout.HorizontalAlignment = Enum.HorizontalAlignment.Center
                Dropdown_ElementContainerOptionsLayout.SortOrder    = Enum.SortOrder.LayoutOrder
                Dropdown_ElementContainerOptionsLayout.Padding      = UDim.new(0, 5)

                Dropdown_ElementText.Size = UDim2.new(0, Dropdown_ElementText.TextBounds.X, 0, 40)

                if (DropdownOptions.Description) then 
                    local Dropdown_ElementDescription = Instance.new("TextLabel")

                    Dropdown_ElementDescription.Name = "Dropdown_ElementDescription"
                    Dropdown_ElementDescription.Parent = Dropdown_Element
                    Dropdown_ElementDescription.AnchorPoint = Vector2.new(0, 0.5)
                    Dropdown_ElementDescription.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                    Dropdown_ElementDescription.BackgroundTransparency = 1.000
                    Dropdown_ElementDescription.Position = UDim2.new(0, Dropdown_ElementText.Size.X.Offset + 20, 0.4, -8)
                    Dropdown_ElementDescription.Size = UDim2.new(0.58759135, 100, 0.53093797, 0)
                    Dropdown_ElementDescription.FontFace = Library.Font
                    Dropdown_ElementDescription.Text = DropdownOptions.Description
                    Dropdown_ElementDescription.TextColor3 = Color3.fromRGB(95, 95, 95)
                    Dropdown_ElementDescription.TextSize = 13.000
                    Dropdown_ElementDescription.TextWrapped = true
                    Dropdown_ElementDescription.TextXAlignment = Enum.TextXAlignment.Left
                    Dropdown_ElementDescription.TextTransparency = 1

                    Dropdown_ElementText.MouseEnter:Connect(function()
                        Tween:Create(Dropdown_ElementDescription, TweenInfo.new(.3), {
                            TextTransparency = 0
                        }):Play()
                    end)

                    Dropdown_ElementText.MouseLeave:Connect(function()
                        Tween:Create(Dropdown_ElementDescription, TweenInfo.new(.3), {
                            TextTransparency = 1
                        }):Play()
                    end)
                end

                local Manager = {}

                local OPEN = false

                local function UpdateDropdown() 
                    if OPEN then
                        Tween:Create(Dropdown_ElementContainer, TweenInfo.new(.3), {
                            Size = UDim2.new(
                                Dropdown_ElementContainer.Size.X.Scale,
                                Dropdown_ElementContainer.Size.X.Offset,
                                Dropdown_ElementContainer.Size.Y.Scale,
                                95
                            )
                        }):Play()
                        Tween:Create(Dropdown_Element, TweenInfo.new(.3), {
                            Size = UDim2.new(
                                Dropdown_Element.Size.X.Scale,
                                Dropdown_Element.Size.X.Offset,
                                Dropdown_Element.Size.Y.Scale,
                                140
                            )
                        }):Play()
                        Tween:Create(Section, TweenInfo.new(.3), {
                            Size = UDim2.new(
                                Section.Size.X.Scale,
                                Section.Size.X.Offset,
                                Section.Size.Y.Scale,
                                SectionOffset + 77
                            )
                        }):Play()
                        Tween:Create(Dropdown_ElementExpandStateIcon, TweenInfo.new(.3), {
                            Rotation = 180
                        }):Play()
                    else
                        Tween:Create(Dropdown_ElementContainer, TweenInfo.new(.3), {
                            Size = UDim2.new(
                                Dropdown_ElementContainer.Size.X.Scale,
                                Dropdown_ElementContainer.Size.X.Offset,
                                Dropdown_ElementContainer.Size.Y.Scale,
                                25
                            )
                        }):Play()
                        Tween:Create(Dropdown_Element, TweenInfo.new(.3), {
                            Size = UDim2.new(
                                Dropdown_Element.Size.X.Scale,
                                Dropdown_Element.Size.X.Offset,
                                Dropdown_Element.Size.Y.Scale,
                                70
                            )
                        }):Play()
                        Tween:Create(Section, TweenInfo.new(.3), {
                            Size = UDim2.new(
                                Section.Size.X.Scale,
                                Section.Size.X.Offset,
                                Section.Size.Y.Scale,
                                Section.Size.Y.Offset - 71
                            )
                        }):Play()
                        Tween:Create(Dropdown_ElementExpandStateIcon, TweenInfo.new(.3), {
                            Rotation = 0
                        }):Play()
                    end
                end

                Dropdown_ElementButton.MouseButton1Click:Connect(function()
                    OPEN = not OPEN
                    UpdateDropdown()
                end)

                Dropdown_ElementExpandStateIcon.MouseButton1Click:Connect(function()
                    OPEN = not OPEN
                    UpdateDropdown()
                end)

                task.spawn(function()
                    local item_classes = {}
                    local items = {}

                    for i, v in pairs(DropdownOptions.Items) do 
                        table.insert(items, v)
                    end 

                    if (#DropdownOptions.Items <= 1) then
                        local Dropdown_Warning        = Instance.new("TextLabel")
                        Dropdown_Warning.Name         = "Dropdown_ElementOptionText"
                        Dropdown_Warning.Parent       = Dropdown_ElementContainerOptions
                        Dropdown_Warning.AnchorPoint  = Vector2.new(0.5, 0.5)
                        Dropdown_Warning.BackgroundColor3 = Color3.fromRGB(18, 18, 18)
                        Dropdown_Warning.BackgroundTransparency = 1.000
                        Dropdown_Warning.Position     = UDim2.new(0.5, 0, 0.5, 0)
                        Dropdown_Warning.Size         = UDim2.new(1, 0, 0, 50)
                        Dropdown_Warning.FontFace     = Library.Font
                        Dropdown_Warning.Text         = "Warning! Items table is empty or contains one item"
                        Dropdown_Warning.TextColor3   = Color3.fromRGB(149, 149, 149)
                        Dropdown_Warning.TextSize     = 14.000
                        Dropdown_Warning.TextXAlignment = Enum.TextXAlignment.Center
                        Dropdown_Warning.TextYAlignment = Enum.TextYAlignment.Center
                        return
                    end
                    
                    for i, item in pairs(items) do 
                        if tostring(item) then
                            local Dropdown_ElementOption            = Instance.new("Frame")
                            local Dropdown_ElementOptionText        = Instance.new("TextLabel")
                            local Dropdown_ElementOptionTextPadding = Instance.new("UIPadding")
                            local Dropdown_ElementOptionCorner      = Instance.new("UICorner")
                            local Dropdown_Stroke                   = Support:CreateStroke(Dropdown_ElementOption, {
                                Color = Color3.fromRGB(30, 30, 30),
                                Thickness = 1,
                                Transparency = 1,
                                Enabled = true
                            })

                            local function UpdateItemStroke(b) 
                                if (b) then
                                    Tween:Create(Dropdown_Stroke, TweenInfo.new(.3), {
                                        Transparency = 0
                                    }):Play()
                                else
                                    Tween:Create(Dropdown_Stroke, TweenInfo.new(.3), {
                                        Transparency = 1
                                    }):Play()
                                end
                            end

                            -- function Support:CreateStroke(Object, Properties) 
                            --     local Stroke = Instance.new("UIStroke", Object);
                            --     Stroke.ApplyStrokeMode = Enum.ApplyStrokeMode.Contextual
                            --     Stroke.Color = Properties.Color
                            --     Stroke.Thickness = Properties.Thickness
                            --     Stroke.Transparency = Properties.Transparency
                            --     Stroke.Enabled = Properties.Enabled
                            --     Stroke.Name = ("%s_Stroke"):format(Object.Name)
                            
                            --     return Stroke
                            -- end

                            Dropdown_ElementOption.Name             = tostring(item)
                            Dropdown_ElementOption.Parent           = Dropdown_ElementContainerOptions
                            Dropdown_ElementOption.BackgroundColor3 = Color3.fromRGB(18, 18, 18)
                            Dropdown_ElementOption.Size             = UDim2.new(0, 430, 0, 24)
                            
                            Dropdown_ElementOptionText.Name         = "Dropdown_ElementOptionText"
                            Dropdown_ElementOptionText.Parent       = Dropdown_ElementOption
                            Dropdown_ElementOptionText.AnchorPoint  = Vector2.new(0.5, 0.5)
                            Dropdown_ElementOptionText.BackgroundColor3 = Color3.fromRGB(18, 18, 18)
                            Dropdown_ElementOptionText.BackgroundTransparency = 1.000
                            Dropdown_ElementOptionText.Position     = UDim2.new(0.5, 0, 0.5, 0)
                            Dropdown_ElementOptionText.Size         = UDim2.new(0, 430, 0, 24)
                            Dropdown_ElementOptionText.FontFace     = Library.Font
                            Dropdown_ElementOptionText.Text         = tostring(item)
                            Dropdown_ElementOptionText.TextColor3   = Color3.fromRGB(149, 149, 149)
                            Dropdown_ElementOptionText.TextSize     = 14.000
                            
                            Dropdown_ElementOptionTextPadding.Name  = "Dropdown_ElementOptionTextPadding"
                            Dropdown_ElementOptionTextPadding.Parent = Dropdown_ElementOptionText
                            Dropdown_ElementOptionTextPadding.PaddingTop = UDim.new(0, 2)
                            
                            Dropdown_ElementOptionCorner.CornerRadius = UDim.new(0, 4)
                            Dropdown_ElementOptionCorner.Name       = "Dropdown_ElementOptionCorner"
                            Dropdown_ElementOptionCorner.Parent     = Dropdown_ElementOption

                            local item_class = {
                                Components = { },
                            }

                            table.insert(item_class, Dropdown_ElementOption);
                            table.insert(item_class, Dropdown_ElementOptionText);
                            table.insert(item_class, Dropdown_ElementOptionTextPadding);
                            table.insert(item_class, Dropdown_ElementOptionCorner);

                            local HOVERED = false

                            Dropdown_ElementOption.MouseEnter:Connect(function()
                                HOVERED = true
                                UpdateItemStroke(true)
                            end)

                            Dropdown_ElementOption.MouseLeave:Connect(function()
                                HOVERED = false
                                UpdateItemStroke(false)
                            end)

                            Library.Connections.DropdownItemClick = UIS.InputBegan:Connect(function(input, gameProcessedEvent)
                                if (input.UserInputType.Name == "MouseButton1" and HOVERED and OPEN) then
                                    Dropdown_ElementSelectedOption.Text = tostring(item)
                                    local suc, req = pcall(DropdownOptions.OnSelected, tostring(item))
                                    if not suc then
                                        error(req)
                                    end
                                end
                            end)

                            table.insert(item_classes, item_class)
                            Dropdown_ElementContainerOptions.CanvasSize = UDim2.new(
                                Dropdown_ElementContainerOptions.CanvasSize.X.Scale,
                                Dropdown_ElementContainerOptions.CanvasSize.X.Offset,
                                Dropdown_ElementContainerOptions.CanvasSize.Y.Scale,
                                Dropdown_ElementContainerOptions.CanvasSize.Y.Offset + 17
                            )
                        end
                    end

                    function Manager:AddNewItem(item) 
                        table.insert(DropdownOptions.Items, item)
                        local Dropdown_ElementOption            = Instance.new("Frame")
                        local Dropdown_ElementOptionText        = Instance.new("TextLabel")
                        local Dropdown_ElementOptionTextPadding = Instance.new("UIPadding")
                        local Dropdown_ElementOptionCorner      = Instance.new("UICorner")
                        local Dropdown_Stroke                   = Support:CreateStroke(Dropdown_ElementOption, {
                            Color = Color3.fromRGB(30, 30, 30),
                            Thickness = 1,
                            Transparency = 1,
                            Enabled = true
                        })

                        local function UpdateItemStroke(b) 
                            if (b) then
                                Tween:Create(Dropdown_Stroke, TweenInfo.new(.3), {
                                    Transparency = 0
                                }):Play()
                            else
                                Tween:Create(Dropdown_Stroke, TweenInfo.new(.3), {
                                    Transparency = 1
                                }):Play()
                            end
                        end

                        Dropdown_ElementOption.Name             = tostring(item)
                        Dropdown_ElementOption.Parent           = Dropdown_ElementContainerOptions
                        Dropdown_ElementOption.BackgroundColor3 = Color3.fromRGB(18, 18, 18)
                        Dropdown_ElementOption.Size             = UDim2.new(0, 430, 0, 24)
                        
                        Dropdown_ElementOptionText.Name         = "Dropdown_ElementOptionText"
                        Dropdown_ElementOptionText.Parent       = Dropdown_ElementOption
                        Dropdown_ElementOptionText.AnchorPoint  = Vector2.new(0.5, 0.5)
                        Dropdown_ElementOptionText.BackgroundColor3 = Color3.fromRGB(18, 18, 18)
                        Dropdown_ElementOptionText.BackgroundTransparency = 1.000
                        Dropdown_ElementOptionText.Position     = UDim2.new(0.5, 0, 0.5, 0)
                        Dropdown_ElementOptionText.Size         = UDim2.new(0, 430, 0, 24)
                        Dropdown_ElementOptionText.FontFace     = Library.Font
                        Dropdown_ElementOptionText.Text         = tostring(item)
                        Dropdown_ElementOptionText.TextColor3   = Color3.fromRGB(149, 149, 149)
                        Dropdown_ElementOptionText.TextSize     = 14.000
                        
                        Dropdown_ElementOptionTextPadding.Name  = "Dropdown_ElementOptionTextPadding"
                        Dropdown_ElementOptionTextPadding.Parent = Dropdown_ElementOptionText
                        Dropdown_ElementOptionTextPadding.PaddingTop = UDim.new(0, 2)
                        
                        Dropdown_ElementOptionCorner.CornerRadius = UDim.new(0, 4)
                        Dropdown_ElementOptionCorner.Name       = "Dropdown_ElementOptionCorner"
                        Dropdown_ElementOptionCorner.Parent     = Dropdown_ElementOption

                        local item_class = {
                            Components = { },
                        }

                        table.insert(item_class, Dropdown_ElementOption);
                        table.insert(item_class, Dropdown_ElementOptionText);
                        table.insert(item_class, Dropdown_ElementOptionTextPadding);
                        table.insert(item_class, Dropdown_ElementOptionCorner);

                        local HOVERED = false

                        Dropdown_ElementOption.MouseEnter:Connect(function()
                            HOVERED = true
                            UpdateItemStroke(true)
                        end)

                        Dropdown_ElementOption.MouseLeave:Connect(function()
                            HOVERED = false
                            UpdateItemStroke(false)
                        end)

                        Library.Connections.DropdownItemClick = UIS.InputBegan:Connect(function(input, gameProcessedEvent)
                            if (input.UserInputType.Name == "MouseButton1" and HOVERED and OPEN) then
                                Dropdown_ElementSelectedOption.Text = tostring(item)
                                local suc, req = pcall(DropdownOptions.OnSelected, tostring(item))
                                if not suc then
                                    error(req)
                                end
                            end
                        end)

                        table.insert(item_classes, item_class)
                        Dropdown_ElementContainerOptions.CanvasSize = UDim2.new(
                            Dropdown_ElementContainerOptions.CanvasSize.X.Scale,
                            Dropdown_ElementContainerOptions.CanvasSize.X.Offset,
                            Dropdown_ElementContainerOptions.CanvasSize.Y.Scale,
                            Dropdown_ElementContainerOptions.CanvasSize.Y.Offset + 17
                        )
                    end

                    function Manager:RemoveItem(item) 
                        table.remove(DropdownOptions.Items, table.find(DropdownOptions.Items, item))
                        for i, v in pairs(Dropdown_ElementContainerOptions:GetChildren()) do 
                            if (v.Name == item) then 
                                v:Destroy()
                            end
                        end 
                    end
                end)

                SectionOffset += 78
                UpdateSection()
                return Manager
            end

            function SectionHandler:CreateMultiDropdown(MultidropdownOptions) 
                local MultidropdownOptions = MultidropdownOptions or {
                    Name = MultidropdownOptions.Name,
                    Description = MultidropdownOptions.Description or "",
                    Items = MultidropdownOptions.Items or { ["1"] = true, ["2"] = false, ["3"] = true },
                    OnChanged = MultidropdownOptions.OnChanged or function (value)
                        print(value)
                    end
                }

                local MultiDropdown_Element                               = Instance.new("Frame")
                local MultiDropdown_ElementCorner                         = Instance.new("UICorner")
                local MultiDropdown_ElementText                           = Instance.new("TextLabel")
                local MultiDropdown_ElementContainer                      = Instance.new("Frame")
                local MultiDropdown_ElementContainerCorner                = Instance.new("UICorner")
                local MultiDropdown_ElementEnabledOptionsContainer        = Instance.new("TextLabel")
                local MultiDropdown_ElementEnabledOptionsContainerPadding = Instance.new("UIPadding")
                local MultiDropdown_ElementExpandStateIcon                = Instance.new("ImageButton")
                local MultiDropdown_ElementToggleAll                      = Instance.new("Frame")
                local MultiDropdown_ElementToggleAllCorner                = Instance.new("UICorner")
                local MultiDropdown_ElementToggleAllButton                = Instance.new("TextButton")
                local MultiDropdown_ElementEnableAll                      = Instance.new("Frame")
                local MultiDropdown_ElementEnableAllCorner                = Instance.new("UICorner")
                local MultiDropdown_ElementEnableAllButton                = Instance.new("TextButton")
                local MultiDropdown_ElementContainerLayout                = Instance.new("UIListLayout")
                local MultiDropdown_ElementOptions                        = Instance.new("ScrollingFrame")
                local MultiDropdown_ElementOptionsLayout                  = Instance.new("UIListLayout")
                local MultiDropdown_ElementOptionsPadding                 = Instance.new("UIPadding")

                MultiDropdown_Element.Name                                = "MultiDropdown_Element"
                MultiDropdown_Element.Parent                              = Section
                MultiDropdown_Element.BackgroundColor3                    = Color3.fromRGB(24, 24, 24)
                MultiDropdown_Element.Position                            = UDim2.new(0.0105263162, 0, 0, 0)
                MultiDropdown_Element.Size                                = UDim2.new(1, -5, 0, 80)

                MultiDropdown_ElementCorner.CornerRadius                  = UDim.new(0, 4)
                MultiDropdown_ElementCorner.Name                          = "MultiDropdown_ElementCorner"
                MultiDropdown_ElementCorner.Parent                        = MultiDropdown_Element

                MultiDropdown_ElementText.Name                            = "MultiDropdown_ElementText"
                MultiDropdown_ElementText.Parent                          = MultiDropdown_Element
                MultiDropdown_ElementText.AnchorPoint                     = Vector2.new(0, 0.5)
                MultiDropdown_ElementText.BackgroundColor3                = Color3.fromRGB(255, 255, 255)
                MultiDropdown_ElementText.BackgroundTransparency          = 1.000
                MultiDropdown_ElementText.Position                        = UDim2.new(0, 15, 0, 20)
                MultiDropdown_ElementText.Size                            = UDim2.new(0, 100, 0, 40)
                MultiDropdown_ElementText.FontFace                        = Library.Font
                MultiDropdown_ElementText.Text                            = MultidropdownOptions.Name
                MultiDropdown_ElementText.TextColor3                      = Color3.fromRGB(225, 225, 225)
                MultiDropdown_ElementText.TextSize                        = 17.000
                MultiDropdown_ElementText.TextXAlignment                  = Enum.TextXAlignment.Left

                MultiDropdown_ElementContainer.Name                       = "MultiDropdown_ElementContainer"
                MultiDropdown_ElementContainer.Parent                     = MultiDropdown_Element
                MultiDropdown_ElementContainer.BackgroundColor3           = Color3.fromRGB(18, 18, 18)
                MultiDropdown_ElementContainer.ClipsDescendants           = true
                MultiDropdown_ElementContainer.Position                   = UDim2.new(0.0253744796, 0, 0, 35)
                MultiDropdown_ElementContainer.Size                       = UDim2.new(0, 439, 0, 33)

                MultiDropdown_ElementContainerCorner.CornerRadius         = UDim.new(0, 4)
                MultiDropdown_ElementContainerCorner.Name                 = "MultiDropdown_ElementContainerCorner"
                MultiDropdown_ElementContainerCorner.Parent               = MultiDropdown_ElementContainer

                MultiDropdown_ElementEnabledOptionsContainer.Name         = "MultiDropdown_ElementEnabledOptionsContainer"
                MultiDropdown_ElementEnabledOptionsContainer.Parent       = MultiDropdown_ElementContainer
                MultiDropdown_ElementEnabledOptionsContainer.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                MultiDropdown_ElementEnabledOptionsContainer.BackgroundTransparency = 1.000
                MultiDropdown_ElementEnabledOptionsContainer.Size         = UDim2.new(1, 0, 0, 26)
                MultiDropdown_ElementEnabledOptionsContainer.FontFace     = Library.Font
                MultiDropdown_ElementEnabledOptionsContainer.Text         = ""
                MultiDropdown_ElementEnabledOptionsContainer.TextColor3   = Color3.fromRGB(206, 206, 206)
                MultiDropdown_ElementEnabledOptionsContainer.TextSize     = 14.000
                MultiDropdown_ElementEnabledOptionsContainer.TextXAlignment = Enum.TextXAlignment.Left

                MultiDropdown_ElementEnabledOptionsContainerPadding.Name   = "MultiDropdown_ElementEnabledOptionsContainerPadding"
                MultiDropdown_ElementEnabledOptionsContainerPadding.Parent = MultiDropdown_ElementEnabledOptionsContainer
                MultiDropdown_ElementEnabledOptionsContainerPadding.PaddingTop = UDim.new(0, 2)

                MultiDropdown_ElementExpandStateIcon.Name                   = "MultiDropdown_ElementExpandStateIcon"
                MultiDropdown_ElementExpandStateIcon.Parent                 = MultiDropdown_ElementEnabledOptionsContainer
                MultiDropdown_ElementExpandStateIcon.BackgroundTransparency = 1.000
                MultiDropdown_ElementExpandStateIcon.Position               = UDim2.new(0.920991659, 0, 0.125000015, -2)
                MultiDropdown_ElementExpandStateIcon.Size                   = UDim2.new(0, 25, 0, 25)
                MultiDropdown_ElementExpandStateIcon.ZIndex                 = 2
                MultiDropdown_ElementExpandStateIcon.Image                  = "rbxassetid://3926305904"
                MultiDropdown_ElementExpandStateIcon.ImageColor3            = Color3.fromRGB(184, 184, 184)
                MultiDropdown_ElementExpandStateIcon.ImageRectOffset        = Vector2.new(564, 284)
                MultiDropdown_ElementExpandStateIcon.ImageRectSize          = Vector2.new(36, 36)

                MultiDropdown_ElementToggleAll.Name                         = "MultiDropdown_ElementToggleAll"
                MultiDropdown_ElementToggleAll.Parent                       = MultiDropdown_ElementEnabledOptionsContainer
                MultiDropdown_ElementToggleAll.BackgroundColor3             = Color3.fromRGB(20, 20, 20)
                MultiDropdown_ElementToggleAll.AnchorPoint                  = Vector2.new(0, 0.5)
                MultiDropdown_ElementToggleAll.Position                     = UDim2.new(0.0205011368, 0, 0.5, 3)
                MultiDropdown_ElementToggleAll.Size                         = UDim2.new(0, 175, 0, 17)

                local MutliDropdown_ElementToggleAllStroke           = Support:CreateStroke(MultiDropdown_ElementToggleAll, {
                    Color = Color3.fromRGB(30, 30, 30),
                    Thickness = 1,
                    Transparency = 0,
                    Enabled = true
                })

                MultiDropdown_ElementToggleAllCorner.CornerRadius           = UDim.new(0, 4)
                MultiDropdown_ElementToggleAllCorner.Name                   = "MultiDropdown_ElementToggleAllCorner"
                MultiDropdown_ElementToggleAllCorner.Parent                 = MultiDropdown_ElementToggleAll

                MultiDropdown_ElementToggleAllButton.Name                   = "MultiDropdown_ElementToggleAllButton"
                MultiDropdown_ElementToggleAllButton.Parent                 = MultiDropdown_ElementToggleAll
                MultiDropdown_ElementToggleAllButton.BackgroundColor3     = Color3.fromRGB(255, 255, 255)
                MultiDropdown_ElementToggleAllButton.BackgroundTransparency = 1.000
                MultiDropdown_ElementToggleAllButton.Position               = UDim2.new(-0.00999994762, 0, 0, 0)
                MultiDropdown_ElementToggleAllButton.Size                   = UDim2.new(1, 0, 1, 0)
                MultiDropdown_ElementToggleAllButton.FontFace               = Library.Font
                MultiDropdown_ElementToggleAllButton.LineHeight             = 0.990
                MultiDropdown_ElementToggleAllButton.Text                   = "Disable All"
                MultiDropdown_ElementToggleAllButton.TextColor3             = Color3.fromRGB(194, 194, 194)
                MultiDropdown_ElementToggleAllButton.TextSize               = 13.000

                --> Here
                MultiDropdown_ElementEnableAll.Name                         = "MultiDropdown_ElementEnableAll"
                MultiDropdown_ElementEnableAll.Parent                       = MultiDropdown_ElementEnabledOptionsContainer
                MultiDropdown_ElementEnableAll.BackgroundColor3             = Color3.fromRGB(20, 20, 20)
                MultiDropdown_ElementEnableAll.AnchorPoint                  = Vector2.new(0, 0.5)
                MultiDropdown_ElementEnableAll.Position                     = UDim2.new(0.45, 0, 0.5, 3)
                MultiDropdown_ElementEnableAll.Size                         = UDim2.new(0, 175, 0, 17)

                local MultiDropdown_ElementEnableAllStroke           = Support:CreateStroke(MultiDropdown_ElementEnableAll, {
                    Color = Color3.fromRGB(30, 30, 30),
                    Thickness = 1,
                    Transparency = 0,
                    Enabled = true
                })

                MultiDropdown_ElementEnableAllCorner.CornerRadius           = UDim.new(0, 4)
                MultiDropdown_ElementEnableAllCorner.Name                   = "MultiDropdown_ElementToggleAllCorner"
                MultiDropdown_ElementEnableAllCorner.Parent                 = MultiDropdown_ElementEnableAll

                MultiDropdown_ElementEnableAllButton.Name                   = "MultiDropdown_ElementToggleAllButton"
                MultiDropdown_ElementEnableAllButton.Parent                 = MultiDropdown_ElementEnableAll
                MultiDropdown_ElementEnableAllButton.BackgroundColor3     = Color3.fromRGB(255, 255, 255)
                MultiDropdown_ElementEnableAllButton.BackgroundTransparency = 1.000
                MultiDropdown_ElementEnableAllButton.Position               = UDim2.new(-0.00999994762, 0, 0, 0)
                MultiDropdown_ElementEnableAllButton.Size                   = UDim2.new(1, 0, 1, 0)
                MultiDropdown_ElementEnableAllButton.FontFace               = Library.Font
                MultiDropdown_ElementEnableAllButton.LineHeight             = 0.990
                MultiDropdown_ElementEnableAllButton.Text                   = "Enable All"
                MultiDropdown_ElementEnableAllButton.TextColor3             = Color3.fromRGB(194, 194, 194)
                MultiDropdown_ElementEnableAllButton.TextSize               = 13.000

                MultiDropdown_ElementContainerLayout.Name                   = "MultiDropdown_ElementContainerLayout"
                MultiDropdown_ElementContainerLayout.Parent                 = MultiDropdown_ElementContainer
                MultiDropdown_ElementContainerLayout.HorizontalAlignment    = Enum.HorizontalAlignment.Center
                MultiDropdown_ElementContainerLayout.SortOrder              = Enum.SortOrder.LayoutOrder
                MultiDropdown_ElementContainerLayout.Padding                = UDim.new(0, 7)

                MultiDropdown_ElementOptions.Name                           = "MultiDropdown_ElementOptions"
                MultiDropdown_ElementOptions.Parent                         = MultiDropdown_ElementContainer
                MultiDropdown_ElementOptions.Active                         = true
                MultiDropdown_ElementOptions.BackgroundColor3               = Color3.fromRGB(22, 22, 22)
                MultiDropdown_ElementOptions.BackgroundTransparency         = 1.000
                MultiDropdown_ElementOptions.BorderSizePixel                = 0
                MultiDropdown_ElementOptions.Position                       = UDim2.new(-3.40597985e-08, 0, 0.177777782, 0)
                MultiDropdown_ElementOptions.Size                           = UDim2.new(1.00000012, 0, 0.348148137, 60)
                MultiDropdown_ElementOptions.CanvasSize                     = UDim2.new(0, 0, 0, 90)
                MultiDropdown_ElementOptions.ScrollBarThickness             = 0

                MultiDropdown_ElementOptionsLayout.Name                     = "MultiDropdown_ElementOptionsLayout"
                MultiDropdown_ElementOptionsLayout.Parent                   = MultiDropdown_ElementOptions
                MultiDropdown_ElementOptionsLayout.HorizontalAlignment      = Enum.HorizontalAlignment.Center
                MultiDropdown_ElementOptionsLayout.SortOrder                = Enum.SortOrder.LayoutOrder
                MultiDropdown_ElementOptionsLayout.Padding                  = UDim.new(0, 5)

                MultiDropdown_ElementOptionsPadding.Name                    = "MultiDropdown_ElementOptionsPadding"
                MultiDropdown_ElementOptionsPadding.Parent                  = MultiDropdown_ElementOptions
                MultiDropdown_ElementOptionsPadding.PaddingBottom           = UDim.new(0, 3)
                MultiDropdown_ElementOptionsPadding.PaddingTop              = UDim.new(0, 7)

                MultiDropdown_ElementText.Size = UDim2.new(0, MultiDropdown_ElementText.TextBounds.X, 0, 40)

                if (MultidropdownOptions.Description) then 
                    local MultiDropdown_ElementDescription = Instance.new("TextLabel")

                    MultiDropdown_ElementDescription.Name = "MultiDropdown_ElementDescription"
                    MultiDropdown_ElementDescription.Parent = MultiDropdown_Element
                    MultiDropdown_ElementDescription.AnchorPoint = Vector2.new(0, 0.5)
                    MultiDropdown_ElementDescription.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                    MultiDropdown_ElementDescription.BackgroundTransparency = 1.000
                    MultiDropdown_ElementDescription.Position = UDim2.new(0, MultiDropdown_ElementText.Size.X.Offset + 20, 0.4, -13)
                    MultiDropdown_ElementDescription.Size = UDim2.new(0.58759135, 100, 0.53093797, 0)
                    MultiDropdown_ElementDescription.FontFace = Library.Font
                    MultiDropdown_ElementDescription.LineHeight = 0.650
                    MultiDropdown_ElementDescription.Text = MultidropdownOptions.Description
                    MultiDropdown_ElementDescription.TextColor3 = Color3.fromRGB(95, 95, 95)
                    MultiDropdown_ElementDescription.TextSize = 13.000
                    MultiDropdown_ElementDescription.TextWrapped = true
                    MultiDropdown_ElementDescription.TextXAlignment = Enum.TextXAlignment.Left
                    MultiDropdown_ElementDescription.TextTransparency = 1

                    MultiDropdown_ElementText.MouseEnter:Connect(function()
                        Tween:Create(MultiDropdown_ElementDescription, TweenInfo.new(.3), {
                            TextTransparency = 0
                        }):Play()
                    end)

                    MultiDropdown_ElementText.MouseLeave:Connect(function()
                        Tween:Create(MultiDropdown_ElementDescription, TweenInfo.new(.3), {
                            TextTransparency = 1
                        }):Play()
                    end)
                end

                local OPEN = false

                local function UpdateDropdown() 
                    if OPEN then
                        Tween:Create(MultiDropdown_ElementContainer, TweenInfo.new(.3), {
                            Size = UDim2.new(
                                MultiDropdown_ElementContainer.Size.X.Scale,
                                MultiDropdown_ElementContainer.Size.X.Offset,
                                MultiDropdown_ElementContainer.Size.Y.Scale,
                                95
                            )
                        }):Play()
                        Tween:Create(MultiDropdown_Element, TweenInfo.new(.3), {
                            Size = UDim2.new(
                                MultiDropdown_Element.Size.X.Scale,
                                MultiDropdown_Element.Size.X.Offset,
                                MultiDropdown_Element.Size.Y.Scale,
                                140
                            )
                        }):Play()
                        Tween:Create(Section, TweenInfo.new(.3), {
                            Size = UDim2.new(
                                Section.Size.X.Scale,
                                Section.Size.X.Offset,
                                Section.Size.Y.Scale,
                                SectionOffset + 70
                            )
                        }):Play()
                        Tween:Create(MultiDropdown_ElementExpandStateIcon, TweenInfo.new(.3), {
                            Rotation = 180
                        }):Play()
                    else
                        Tween:Create(MultiDropdown_Element, TweenInfo.new(.3), {
                            Size = UDim2.new(
                                MultiDropdown_Element.Size.X.Scale,
                                MultiDropdown_Element.Size.X.Offset,
                                MultiDropdown_Element.Size.Y.Scale,
                                80
                            )
                        }):Play()
                        Tween:Create(MultiDropdown_ElementContainer, TweenInfo.new(.3), {
                            Size = UDim2.new(
                                MultiDropdown_ElementContainer.Size.X.Scale,
                                MultiDropdown_ElementContainer.Size.X.Offset,
                                MultiDropdown_ElementContainer.Size.Y.Scale,
                                33
                            )
                        }):Play()
                        Tween:Create(Section, TweenInfo.new(.3), {
                            Size = UDim2.new(
                                Section.Size.X.Scale,
                                Section.Size.X.Offset,
                                Section.Size.Y.Scale,
                                Section.Size.Y.Offset + SectionY - 70
                            )
                        }):Play()
                        Tween:Create(MultiDropdown_ElementExpandStateIcon, TweenInfo.new(.3), {
                            Rotation = 0
                        }):Play()
                    end
                end

                MultiDropdown_ElementExpandStateIcon.MouseButton1Click:Connect(function()
                    OPEN = not OPEN
                    UpdateDropdown()
                end)

                task.spawn(function() 
                    local item_classes = {}

                    local option_order = {}
                    for item, _ in pairs(MultidropdownOptions.Items) do 
                        table.insert(option_order, item)
                    end 

                    table.sort(option_order)

                    for _, item in pairs(option_order) do 
                        if (tostring(item)) then
                            local MultiDropdown_ElementBooleanOption                  = Instance.new("Frame")
                            local MultiDropdown_ElementBooleanOptionText              = Instance.new("TextLabel")
                            local MultiDropdown_ElementBooleanOptionTextPadding       = Instance.new("UIPadding")
                            local MultiDropdown_ElementBooleanOptionCorner            = Instance.new("UICorner")
                            local MutliDropdown_ElementBooleanOptionStroke            = Support:CreateStroke(MultiDropdown_ElementBooleanOption, {
                                                                                            Color = Color3.fromRGB(30, 30, 30),
                                                                                            Thickness = 1,
                                                                                            Transparency = 0,
                                                                                            Enabled = true
                                                                                        })
                            local MutliDropdown_ElementBooleanOptionButton            = Support:CreateButton(MultiDropdown_ElementBooleanOption)

                            MultiDropdown_ElementBooleanOption.Name                     = "MultiDropdown_ElementBooleanOption"
                            MultiDropdown_ElementBooleanOption.Parent                   = MultiDropdown_ElementOptions
                            MultiDropdown_ElementBooleanOption.BackgroundColor3         = Color3.fromRGB(18, 18, 18)
                            MultiDropdown_ElementBooleanOption.Position                 = UDim2.new(0.0102506373, 0, 0.0654205605, 0)
                            MultiDropdown_ElementBooleanOption.Size                     = UDim2.new(0, 422, 0, 24)

                            MultiDropdown_ElementBooleanOptionText.Name                 = "MultiDropdown_ElementBooleanOptionText"
                            MultiDropdown_ElementBooleanOptionText.Parent               = MultiDropdown_ElementBooleanOption
                            MultiDropdown_ElementBooleanOptionText.AnchorPoint          = Vector2.new(0.5, 0.5)
                            MultiDropdown_ElementBooleanOptionText.BackgroundColor3     = Color3.fromRGB(18, 18, 18)
                            MultiDropdown_ElementBooleanOptionText.BackgroundTransparency = 1.000
                            MultiDropdown_ElementBooleanOptionText.Position             = UDim2.new(0.5, 0, 0.5, 0)
                            MultiDropdown_ElementBooleanOptionText.Size                 = UDim2.new(0, 430, 0, 24)
                            MultiDropdown_ElementBooleanOptionText.FontFace             = Library.Font
                            MultiDropdown_ElementBooleanOptionText.Text                 = tostring(item)
                            MultiDropdown_ElementBooleanOptionText.TextColor3           = Color3.fromRGB(149, 149, 149)
                            MultiDropdown_ElementBooleanOptionText.TextSize             = 14.000

                            MultiDropdown_ElementBooleanOptionTextPadding.Name          = "MultiDropdown_ElementBooleanOptionTextPadding"
                            MultiDropdown_ElementBooleanOptionTextPadding.Parent        = MultiDropdown_ElementBooleanOptionText
                            MultiDropdown_ElementBooleanOptionTextPadding.PaddingTop    = UDim.new(0, 2)

                            MultiDropdown_ElementBooleanOptionCorner.CornerRadius       = UDim.new(0, 4)
                            MultiDropdown_ElementBooleanOptionCorner.Name               = "MultiDropdown_ElementBooleanOptionCorner"
                            MultiDropdown_ElementBooleanOptionCorner.Parent             = MultiDropdown_ElementBooleanOption

                            local OptionEnabled = MultidropdownOptions.Items[item]

                            if not (type(OptionEnabled) == "boolean") then
                                return
                            end
                            
                            local function UpdateOnHover(B) 
                                if (B) then
                                    if (OptionEnabled) then
                                        Tween:Create(MutliDropdown_ElementBooleanOptionStroke, TweenInfo.new(.2), {
                                            Color = Color3.fromRGB(206, 61, 4),
                                        }):Play()
                                    else
                                        Tween:Create(MutliDropdown_ElementBooleanOptionStroke, TweenInfo.new(.2), {
                                            Color = Color3.fromRGB(35, 35, 35),
                                        }):Play()
                                    end
                                else
                                    if (OptionEnabled) then
                                        Tween:Create(MutliDropdown_ElementBooleanOptionStroke, TweenInfo.new(.2), {
                                            Color = Color3.fromRGB(248, 72, 4),
                                        }):Play()
                                    else
                                        Tween:Create(MutliDropdown_ElementBooleanOptionStroke, TweenInfo.new(.2), {
                                            Color = Color3.fromRGB(30, 30, 30),
                                        }):Play()
                                    end
                                end
                            end

                            local function UpdateOption() 
                                if (OptionEnabled) then
                                    Tween:Create(MutliDropdown_ElementBooleanOptionStroke, TweenInfo.new(.2), {
                                        Color = Color3.fromRGB(248, 72, 4),
                                    }):Play()
                                else
                                    Tween:Create(MutliDropdown_ElementBooleanOptionStroke, TweenInfo.new(.2), {
                                        Color = Color3.fromRGB(30, 30, 30),
                                    }):Play()
                                end
                            end

                            local item_class = {
                                Enabled = OptionEnabled
                            }

                            pcall(UpdateOption)

                            MultiDropdown_ElementBooleanOption.MouseEnter:Connect(function() UpdateOnHover(true) end)
                            MultiDropdown_ElementBooleanOption.MouseLeave:Connect(function() UpdateOnHover(false) end)

                            MutliDropdown_ElementBooleanOptionButton.MouseButton1Click:Connect(function()
                                if (MutliDropdown_ElementBooleanOptionButton) then
                                    OptionEnabled = not OptionEnabled
                                    local suc, req = pcall(MultidropdownOptions.OnChanged, tostring(item), OptionEnabled)
                                    if not suc then
                                        error(req)
                                    else
                                        UpdateOption()

                                        if (table.find(item_classes, item_class)) then
                                            item_classes[table.find(item_classes, item_class)].Enabled = OptionEnabled
                                        end
                                    end
                                end
                            end)

                            item_class.Update = function() 
                                OptionEnabled = item_class.Enabled
                                local suc, req = pcall(MultidropdownOptions.OnChanged, tostring(item), item_class.Enabled)
                                if not suc then
                                    error(req)
                                else
                                    if (item_class.Enabled) then
                                        Tween:Create(MutliDropdown_ElementBooleanOptionStroke, TweenInfo.new(.2), {
                                            Color = Color3.fromRGB(248, 72, 4),
                                        }):Play()
                                    else
                                        Tween:Create(MutliDropdown_ElementBooleanOptionStroke, TweenInfo.new(.2), {
                                            Color = Color3.fromRGB(30, 30, 30),
                                        }):Play()
                                    end
                                end
                            end

                            table.insert(item_classes, item_class)
                            MultiDropdown_ElementOptions.CanvasSize = UDim2.new(
                                MultiDropdown_ElementOptions.CanvasSize.X.Scale,
                                MultiDropdown_ElementOptions.CanvasSize.X.Offset,
                                MultiDropdown_ElementOptions.CanvasSize.Y.Scale,
                                MultiDropdown_ElementOptions.CanvasSize.Y.Offset + 17
                            )
                        end
                    end

                    MultiDropdown_ElementToggleAllButton.MouseButton1Click:Connect(function()
                        for i, item in pairs(item_classes) do 
                            if (item.Enabled) then 
                                item.Enabled = false
                                item.Update()
                            end  
                        end
                    end)

                    MultiDropdown_ElementEnableAllButton.MouseButton1Click:Connect(function()
                        for i, item in pairs(item_classes) do 
                            if not (item.Enabled) then 
                                item.Enabled = true
                                item.Update()
                            end  
                        end
                    end)
                end)

                SectionOffset += 78
                UpdateSection()
            end

            function SectionHandler:CreateAction(ActionOptions) 
                local ActionOptions = ActionOptions or {
                    Name = ActionOptions.Name or "Action 1",
                    OnClick = ActionOptions.OnClick or function() 
                        print("hello")
                    end
                }

                local Action_Element                = Instance.new("Frame")
                local Action_ElementCorner          = Instance.new("UICorner")
                local Action_ElementActivator       = Instance.new("Frame")
                local Action_ElementActivatorCorner = Instance.new("UICorner")
                local Action_ElementText            = Instance.new("TextLabel")
                
                Action_Element.Name                 = "Action_Element"
                Action_Element.Parent               = Section
                Action_Element.BackgroundColor3     = Color3.fromRGB(24, 24, 24)
                Action_Element.Position             = UDim2.new(0, 0, 0.428571433, 0)
                Action_Element.Size                 = UDim2.new(1, -5, 0, 40)
                
                Action_ElementCorner.CornerRadius   = UDim.new(0, 4)
                Action_ElementCorner.Name           = "Action_ElementCorner"
                Action_ElementCorner.Parent         = Action_Element
                
                Action_ElementActivator.Name        = "Action_ElementActivator"
                Action_ElementActivator.Parent      = Action_Element
                Action_ElementActivator.AnchorPoint = Vector2.new(0.5, 0.5)
                Action_ElementActivator.BackgroundColor3 = Color3.fromRGB(18, 18, 18)
                Action_ElementActivator.Position    = UDim2.new(0.5, 0, 0.5, 0)
                Action_ElementActivator.Size        = UDim2.new(1, -15, 1, -12)
                
                Action_ElementActivatorCorner.CornerRadius = UDim.new(0, 4)
                Action_ElementActivatorCorner.Name  = "Action_ElementActivatorCorner"
                Action_ElementActivatorCorner.Parent= Action_ElementActivator
                
                Action_ElementText.Name             = "Action_ElementText"
                Action_ElementText.Parent           = Action_ElementActivator
                Action_ElementText.AnchorPoint      = Vector2.new(0.5, 0.5)
                Action_ElementText.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                Action_ElementText.BackgroundTransparency = 1.000
                Action_ElementText.Position         = UDim2.new(0.5, 0, 0.5, 0)
                Action_ElementText.Size             = UDim2.new(0, 100, 1, 0)
                Action_ElementText.FontFace         = Library.Font
                Action_ElementText.Text             = ActionOptions.Name
                Action_ElementText.TextColor3       = Color3.fromRGB(225, 225, 225)
                Action_ElementText.TextSize         = 17.000

                local Action_ElementButton = Support:CreateButton(Action_ElementActivator)
                Action_ElementButton.ClipsDescendants = true

                Action_ElementButton.MouseButton1Click:Connect(function()
                    local suc, req = pcall(ActionOptions.OnClick)
                    if not suc then
                        error(req)
                    end
                end)

                Action_ElementActivator.MouseEnter:Connect(function()
                    Tween:Create(Action_ElementActivator, TweenInfo.new(.1), {
                        BackgroundColor3 = Color3.fromRGB(19, 19, 19)
                    }):Play()
                end)

                Action_ElementActivator.MouseLeave:Connect(function()
                    Tween:Create(Action_ElementActivator, TweenInfo.new(.1), {
                        BackgroundColor3 = Color3.fromRGB(18, 18, 18)
                    }):Play()
                end)

                SectionOffset += 45
                UpdateSection()
            end

            Section:GetPropertyChangedSignal("Size"):Connect(function()
                Tab.CanvasSize = UDim2.new(Tab.CanvasSize.X.Scale, Tab.CanvasSize.X.Offset, 0, Tab.CanvasSize.Y.Offset + 30)
            end)

            task.defer(function() 
                Tab.CanvasSize = UDim2.new(Tab.CanvasSize.X.Scale, Tab.CanvasSize.X.Offset, 0, Tab.CanvasSize.Y.Offset + Section.Size.Y.Offset + 40)
            end)

            return SectionHandler
        end

        local TabActivator = Support:CreateButton(TabButton)

        PageClass.Update = function() 
            if (PageClass.Showing) then
                Tab.Visible = true

                Tween:Create(TabButton, TweenInfo.new(.3), {
                    BackgroundTransparency = 0.25
                }):Play()
                Tween:Create(TabButtonIndicator, TweenInfo.new(.3), {
                    Size = UDim2.new(0, 2, 1, 0)
                }):Play()
            else
                Tab.Visible = false

                Tween:Create(TabButton, TweenInfo.new(.3), {
                    BackgroundTransparency = 1
                }):Play()
                Tween:Create(TabButtonIndicator, TweenInfo.new(.3), {
                    Size = UDim2.new(0, 2, 0, 0)
                }):Play()
            end
        end

        TabActivator.MouseButton1Click:Connect(function()
            if not (PageClass.Showing) then
                for i, page in pairs(Pages) do 
                    if (page.Name ~= PageClass.Name) then
                        page.Showing = false
                        page.Update()
                    end
                end  

                PageClass.Showing = true
                PageClass.Update()
            end
        end)

        table.insert(Pages, PageClass)

        return ElementHandler
    end
    
    task.defer(function()
        for i, page in pairs(Pages) do 
            if i == 1 then
                if not (page.Showing) then
                    page.Showing = true
                    page.Update()
                end
            else
                page.Showing = false
                page.Update()
            end
        end

        local TweenService = game:GetService("TweenService")
        local UserInputService = game:GetService("UserInputService")
        
        local window = Window
        local topPanel = WindowPanel
        
        local dragging = false
        local dragInput = nil
        local dragStart = nil
        local startPos = nil
        
        local function updateDrag(input)
            local delta = input.Position - dragStart
            local newPosition = UDim2.new(startPos.X.Scale, startPos.X.Offset + delta.X, startPos.Y.Scale, startPos.Y.Offset + delta.Y)
        
            local tweenInfo = TweenInfo.new(0.1, Enum.EasingStyle.Quad, Enum.EasingDirection.Out)
            local tween = TweenService:Create(window, tweenInfo, { Position = newPosition })
            tween:Play()
        end
        
        Library.Connections.PanelBegan = topPanel.InputBegan:Connect(function(input)
            if input.UserInputType == Enum.UserInputType.MouseButton1 then
                dragging = true
                dragStart = input.Position
                startPos = window.Position
        
                input.Changed:Connect(function()
                    if input.UserInputState == Enum.UserInputState.End then
                        dragging = false
                    end
                end)
            end
        end)
        
        Library.Connections.PanelChanged = topPanel.InputChanged:Connect(function(input)
            if input.UserInputType == Enum.UserInputType.MouseMovement then
                dragInput = input
            end
        end)
        
        Library.Connections.DragBegin = UserInputService.InputChanged:Connect(function(input)
            if input == dragInput and dragging then
                updateDrag(input)
            end
        end)

        local destroy
        destroy = UIS.InputBegan:Connect(function(input)
            if input.KeyCode.Name == WindowOptions.DestroyKey then
                for i, obj in pairs(CoreGui:GetChildren()) do 
                    if (obj:IsA("ScreenGui") and obj.Name == Library.Name) then
                        obj:Destroy()
                    end
                end

                for i, connection in pairs(Library.Connections) do 
                    if connection then
                        connection:Disconnect()
                    end
                end  
            end
        end)

        local toggle_ui
        toggle_ui = UIS.InputBegan:Connect(function(input)
            if input.KeyCode.Name == WindowOptions.ToggleKey then
                Window.Visible = not Window.Visible 
            end
        end)
    end)

    return PageHandler
end

return Library
