# Cerberus



## Getting started

```lua
local Library = loadstring(game:HttpGet("https://gitlab.com/blinxduh/cerberus/-/raw/main/Library.lua", true))()
```

## Creating a Window
```lua
local Window = Library:BeginWindow({
    Title = "Title",
    ToggleKey = "RightShift",
    DestroyKey = "RightBracket"
})
```

## Creating a Page
```lua
local Page = Window:BeginPage("<page-name>")
```

## Creating a Section
```lua
local Section = Page:CreateSection("Section 1")
```

## Creating Components

### Action / Button
```lua
local action1 = Section:CreateAction({
    Name = "Action 1",
    OnClick = function() 
        print("Hello, world!")
    end
})
```

### Boolean / Toggle

* There are two styles, "Switch" and "Checkbox"

```lua
-- Switch
local switch = Section:CreateSwitch({
    Name = "Switch 1",
    Default = true,
    OnChanged = function(value) 
        print(value)
    end
})
```
```lua
-- Checkbox
local checkbox = Section:CreateCheckbox({
    Name = "Checkbox 1",
    Default = true,
    OnChanged = function(value) 
        print(value)
    end
})
```

### Sliders / Number
```lua
local slider = Section:CreateSlider({
    Name = "WalkSpeed",
    Range = { 16, 500 },
    Default = { 16 },
    OnChanged = function(value) 
        game:GetService("Players").LocalPlayer.Character.Humanoid.WalkSpeed = value
    end
})
```

* Sliders have an option for keybinds.
```lua
local slider = Section:CreateSlider({
    Name = "WalkSpeed",
    Description = "Modify your walkspeed",
    Range = { 16, 500 },
    Default = { 16 },
    Keybind = "M",
    OnChanged = function(value, enabled)
        if (enabled) then 
            game:GetService("Players").LocalPlayer.Character.Humanoid.WalkSpeed = value
        else
            game:GetService("Players").LocalPlayer.Character.Humanoid.WalkSpeed = 16
        end
    end
})
```

### Dropdowns / List
```lua
local dropdown = Section:CreateDropdown({
    Name = "Type",
    Items = { "Type 1", "Type 2", "Type 3" },
    OnSelected = function(item) 
        print(item, "Selected")
    end
})
```

### Multi-dropdowns / Multi-booleans
```lua
local multidropdown = Section:CreateMultiDropdown({
    Name = "Flags",
    Items = {
        ["Flag 1"] = true,
        ["Flag 2"] = false,
        ["Flag 3"] = true,
        ["Flag 4"] = false,
    },
    OnChanged = function(flag, value) 
        print(flag, ":", value)
    end
})
```

## Methods

### Toggles
```lua
...
toggle1:Set(true)
```
```lua
...
if (toggle1:IsEnabled()) then 
    ...
end
```

### Sliders
```lua
...
slider1:Set(100)
```
```lua
...
if (slider1:GetValue() > 100) then 
    ...
end
```

### Dropdowns
```lua
...
dropdown:AddNewItem("Item 2")
```
```lua
...
dropdown:RemoveItem("Item 2")
```
## Notes

* When adding `keybinds` to sliders, the callback function will pass the value and the state of slider.

* On startup, keybinds may not initiate, to fix this just edit the `keybind`
